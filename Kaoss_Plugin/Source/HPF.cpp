/*
  ==============================================================================

    HPF.cpp
    Created: 17 Dec 2021 12:22:40pm
    Author:  ClarVik

  ==============================================================================
*/

#include "HPF.h"

namespace effects {

    void HPF::updateParams(int x, int y)
    {
        filter.setCutoffFrequency(1. + (y / 100.) * 5000.);
        filter.setResonance(.3 + (x / 100.) * 2.);
    }

    void HPF::prepare(const ProcessSpec& s)
    {
        AudioEffectBase::prepare(s);
        filter.setType(StateVariableTPTFilterType::highpass);
        filter.prepare(s);
    }

}

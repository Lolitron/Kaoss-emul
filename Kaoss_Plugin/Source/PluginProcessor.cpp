/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include "BeatDetector.h"
#include "UDPServer.h"


//==============================================================================
HandlerProcessor::HandlerProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
    : AudioProcessor(BusesProperties()
#if ! JucePlugin_IsMidiEffect
#if ! JucePlugin_IsSynth
        .withInput("Input", juce::AudioChannelSet::stereo(), true)
#endif
        .withOutput("Output", juce::AudioChannelSet::stereo(), true)
#endif
    )
#endif
{
    rate.setSmoothed(std::make_pair(false,false));
    tempo.setSmoothed(std::make_pair(false,false));
    pitch.setSmoothed(std::make_pair(false,false));
    looper.setSmoothed(std::make_pair(false,false));
    rlooper.setSmoothed(std::make_pair(false, false));
    delay.setSmoothed(std::make_pair(true, false));
    tape.setSmoothed(std::make_pair(true, false));
    sDelay.setSmoothed(std::make_pair(false, false));

    mpFFTprocessor = new FFT(10);
    mEffectRack.push_back(&rate);
    mEffectRack.push_back(&tempo);
    mEffectRack.push_back(&pitch);
    mEffectRack.push_back(&delay);
    mEffectRack.push_back(&sDelay);
    mEffectRack.push_back(&tape);
    mEffectRack.push_back(&looper);
    mEffectRack.push_back(&rlooper);
    mEffectRack.push_back(&chorus);
    mEffectRack.push_back(&decimator);
    mEffectRack.push_back(&flanger);
    mEffectRack.push_back(&rever);
    mEffectRack.push_back(&comp);
    mEffectRack.push_back(&pan);
    mEffectRack.push_back(&hpf);
    mEffectRack.push_back(&lpf);
    mEffectRack.push_back(&bpf);

    startTimerHz(60);
    server = new UDPServer(this);

}

HandlerProcessor::~HandlerProcessor()
{
}

//==============================================================================
const juce::String HandlerProcessor::getName() const
{
    return JucePlugin_Name;
}

bool HandlerProcessor::acceptsMidi() const
{
#if JucePlugin_WantsMidiInput
    return true;
#else
    return false;
#endif
}

bool HandlerProcessor::producesMidi() const
{
#if JucePlugin_ProducesMidiOutput
    return true;
#else
    return false;
#endif
}

bool HandlerProcessor::isMidiEffect() const
{
#if JucePlugin_IsMidiEffect
    return true;
#else
    return false;
#endif
}

double HandlerProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int HandlerProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int HandlerProcessor::getCurrentProgram()
{
    return 0;
}

void HandlerProcessor::setCurrentProgram(int index)
{
}

const juce::String HandlerProcessor::getProgramName(int index)
{
    return {};
}

void HandlerProcessor::changeProgramName(int index, const juce::String& newName)
{
}

//==============================================================================
void HandlerProcessor::prepareToPlay(double sampleRate, int samplesPerBlock)
{
    ProcessSpec spec = { sampleRate, static_cast<juce::uint32> (samplesPerBlock), getTotalNumInputChannels() };

    mSampleRate = 44100;
    mpBeatDetector = new BeatDetector(this);
    mpFftDataL = new float[fftSize * 2];
    mpFftDataR = new float[fftSize * 2];

    for (int i=0;i<mEffectRack.size();i++)
        mEffectRack[i]->prepare(spec);

    x.reset(sampleRate,.3);
    y.reset(sampleRate,.3);
    x.setTargetValue(0);
    y.setTargetValue(0);
}

void HandlerProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool HandlerProcessor::isBusesLayoutSupported(const BusesLayout& layouts) const
{
#if JucePlugin_IsMidiEffect
    juce::ignoreUnused(layouts);
    return true;
#else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    // Some plugin hosts, such as certain GarageBand versions, will only
    // load plugins that support stereo bus layouts.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
        && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
#if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
#endif


    return true;
#endif
}
#endif

void HandlerProcessor::processBlock(juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    int xN = x.getNextValue();
    x.skip(buffer.getNumSamples());
    int yN= y.getNextValue();
    y.skip(buffer.getNumSamples());

    for (int i = getTotalNumInputChannels(); i < getTotalNumOutputChannels(); i++)
        buffer.clear(i, 0, buffer.getNumSamples());

    for (int i = 0; i < mEffectRack.size(); i++)
    {
        if (!mEffectRack[i]->isOn()) continue;

        if (mEffectRack[i]->isRecorded()) 
            mEffectRack[i]->updateParams(mEffectRack[i]->getRecordedCoords().first,mEffectRack[i]->getRecordedCoords().second);
        else
            mEffectRack[i]->updateParams(mEffectRack[i]->isSmoothedX()? xN: x.getTargetValue(), mEffectRack[i]->isSmoothedY() ? yN : y.getTargetValue());

        mEffectRack[i]->process(buffer);
    }

    buffer.applyGain(mGain);

    peak = false;
    for (int channel = 0; channel < getTotalNumInputChannels(); channel++)
    {
        auto* channelData = buffer.getWritePointer(channel);

        for (int i = 0; i < buffer.getNumSamples(); i++)
        {
            if (abs(channelData[i]) >= 0.95)
                peak = true;

            pushToFifo(channel,channelData[i]);
        }
    }
    server->sendPeakAsync();
}

void HandlerProcessor::pushToFifo(int channel, float sample)
{
    switch (channel)
    {
    case 0:
    {
        if (mFifoIndexL == fftSize)
        {
            if (!mNextBlockReady)
            {
                zeromem(mpFftDataL,sizeof(mpFftDataL));
                memcpy(mpFftDataL,mFifoL,sizeof(mFifoL));
            }
            mFifoIndexL = 0;
        }
        mFifoL[mFifoIndexL++] = sample;
        break;
    }
    case 1:
    {
        if (mFifoIndexR == fftSize)
        {
            if (!mNextBlockReady)
            {
                zeromem(mpFftDataR, sizeof(mpFftDataR));
                memcpy(mpFftDataR, mFifoR, sizeof(mFifoR));
                mNextBlockReady = true;
            }
            mFifoIndexR = 0;
        }
        mFifoR[mFifoIndexR++] = sample;
        break;
    }
    default:
        break;
    }
}

void HandlerProcessor::normalizeFftData(float* data)
{
    for (int i = 0; i < fftSize; i++)
    {
        if (data[i] > mMaxAbs)
            mMaxAbs = data[i];
        if (data[i] < mMinAbs)
            mMinAbs = data[i];
    }

    for (int i = 0; i < fftSize; i++) {
        data[i] = (data[i] - mMinAbs) / (mMaxAbs - mMinAbs);
    }
}


void HandlerProcessor::askRefresh()
{
    server->askRefreshAsync();
}

//==============================================================================
bool HandlerProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* HandlerProcessor::createEditor()
{
    return new HandlerProcessorEditor(*this);
    //return new GenericAudioProcessorEditor(*this);
}

//==============================================================================
void HandlerProcessor::getStateInformation(juce::MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void HandlerProcessor::setStateInformation(const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
void HandlerProcessor::timerCallback()
{
    if (mNextBlockReady)
    {
        mpFFTprocessor->performFrequencyOnlyForwardTransform(mpFftDataL);
        mpFFTprocessor->performFrequencyOnlyForwardTransform(mpFftDataR);

        mMinAbs = 100000;
        mMaxAbs = -100000;

        normalizeFftData(mpFftDataL);
        normalizeFftData(mpFftDataR);
        mpBeatDetector->run();
        mNextBlockReady = false;
    }

    server->recieveAsync();
    server->sendBeatAsync();
    effects::AudioEffectBase::beat  = this->beat;
}

void HandlerProcessor::setXY(int x, int y)
{
    this->x.setTargetValue(x);
    this->y.setTargetValue(y);
}


void HandlerProcessor::setOn(int num, bool on)
{
    mEffectRack[num]->setOn(on);
    if(!on) mEffectRack[num]->reset();
}

bool HandlerProcessor::getEffectState(int num)
{
    return mEffectRack[num]->isOn();
}

void HandlerProcessor::applyNewBpm(int newBpm)
{
    effects::AudioEffectBase::bpm = newBpm;
}



//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new HandlerProcessor();
}

/*
  ==============================================================================

    TapeEcho.h
    Created: 16 Jan 2022 9:38:57pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "Essentials.h"
#include "Delay.h"
#include "BPF.h"
#include "LPF.h"

namespace effects {

    class TapeEcho : public Delay
    {
    public:

        void prepare(const ProcessSpec& s) override;

        void process(AudioBuffer<float>& buffer) override;


    private:

        StateVariableTPTFilter<float> mFilter;
    };

}
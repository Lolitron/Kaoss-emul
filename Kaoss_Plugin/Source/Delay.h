/*
  ==============================================================================

    DelayCustom.h
    Created: 19 Dec 2021 12:44:37pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "Essentials.h"

namespace effects {

    class Delay : public AudioEffectBase
    {
    public:
        void updateParams(int x, int y) override;
        virtual void prepare(const ProcessSpec& s) override;
        virtual void process(AudioBuffer<float>& buffer) override;

    protected:

        AudioSampleBuffer delayBuffer;
        int mDelayFreq{ 0 };
        int mWritePosition{ 0 };
        int mReadPosition{ 0 };
        float mFeedback{ 0.5f };

        const float* mpBufferData;
        float* mpDelayBufferData;
        float* mpDryBufferData; //data to write feedback into
    };

}
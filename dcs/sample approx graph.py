import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import numpy as np
import math


def draw_sample_approx():
    x = np.linspace(0, 2.8 * np.pi, 19)
    y = [0.5 * (math.sin(arg) + 1) for arg in x]

    fig, ax = plt.subplots()

    ax.plot(x, y, linestyle="dashed", linewidth=0.5)
    ax.stem(x, y, label="samples", basefmt="grey", markerfmt="C0o", linefmt="C0--")
    ax.step(x, y, color="C0", where="post")
    pts_idx = []
    for i in range(len(y)):
        pts_idx.append(i) if i%6==0 else 0
    ax.add_line(Line2D([x[idx] for idx in pts_idx],[y[idx] for idx in pts_idx], color="green", ms=12 ))

    for i in range(len(pts_idx)-1):
        k = (y[pts_idx[i]] - y[pts_idx[i+1]]) / (x[pts_idx[i]] - x[pts_idx[i+1]])
        b = y[pts_idx[i+1]] - k * x[pts_idx[i+1]]
        y2 = []
        for arg in x[pts_idx[i]:pts_idx[i+1]+1]:
            y2.append(k * arg + b)
        ax.stem(x[pts_idx[i]:pts_idx[i+1]+1], y2, label="approx samples" if i==0 else "", markerfmt="C2o", linefmt="C2--", basefmt="grey")
        ax.step(x[pts_idx[i]:pts_idx[i+1]+1], y2, color="green", where="post")

    ax.set_title("Sample approximation")
    ax.xaxis.set_ticks([])
    ax.yaxis.set_ticks(np.arange(0, 1.01, 0.1))
    ax.set_ylabel("Sample value")
    ax.text(4, 0.75, 'every 6th left', style='italic',
            bbox={'facecolor': 'red', 'alpha': 0.5, 'pad': 10})

    plt.legend()
    plt.show()


def draw_zero_samples():
    x = np.linspace(0.5 * np.pi, 2.5 * np.pi, 25)
    y0 = [0.5 * (math.sin(arg) + 1) for arg in x]
    y = [0.5 * (math.sin(x[0]) + 1)]
    c = 0
    for arg in x[1:]:
        if c < 6:
            y.append(0)
            c += 1
        else:
            c = 0
            y.append(0.5 * (math.sin(arg) + 1))

    fig, ax = plt.subplots()

    ax.plot(x, y0, linestyle="dashed", linewidth=0.5, label="input")
    ax.stem(x, y, markerfmt="C2o", basefmt="grey")
    ax.step(x, y, color="green", where="post", label="output")

    ax.text(4, 0.75, 'every 6th left', style='italic',
            bbox={'facecolor': 'red', 'alpha': 0.5, 'pad': 10})
    ax.set_title("Setting samples to zero")
    ax.xaxis.set_ticks([])
    ax.yaxis.set_ticks(np.arange(0, 1.01, 0.1))
    ax.set_ylabel("Sample value")

    plt.legend()
    plt.show()


def draw_last_samples():
    x = np.linspace(0.5 * np.pi, 2.5 * np.pi, 25)
    y0 = [0.5 * (math.sin(arg) + 1) for arg in x]
    y = [0.5 * (math.sin(x[0]) + 1)]
    c = 0
    last = y[0]
    for arg in x[1:]:
        if c < 6:
            y.append(last)
            c += 1
        else:
            c = 0
            last = 0.5 * (math.sin(arg) + 1)
            y.append(0.5 * (math.sin(arg) + 1))

    fig, ax = plt.subplots()

    ax.plot(x, y0, linestyle="dashed", linewidth=0.5, label="input")
    ax.stem(x, y, markerfmt="C2o", basefmt="grey")
    ax.step(x, y, color="green", where="post", label="output")

    ax.text(4, 0.75, 'every 6th left', style='italic',
            bbox={'facecolor': 'red', 'alpha': 0.5, 'pad': 10})
    ax.set_title("Setting samples to last left")
    ax.xaxis.set_ticks([])
    ax.yaxis.set_ticks(np.arange(0, 1.01, 0.1))
    ax.set_ylabel("Sample value")

    plt.legend()
    plt.show()


if __name__ == "__main__":
    draw_zero_samples()

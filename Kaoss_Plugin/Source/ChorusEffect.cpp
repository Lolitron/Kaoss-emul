/*
  ==============================================================================

    ChorusEffect.cpp
    Created: 17 Dec 2021 11:34:04am
    Author:  ClarVik

  ==============================================================================
*/

#include "ChorusEffect.h"

namespace effects {

    void ChorusEffect::updateParams(int x, int y)
    {
        mProcessor.setMix(y / 100.);
        mProcessor.setDepth(x / 100.);
        mProcessor.setFeedback(-0.5);
    }

    void ChorusEffect::prepare(const ProcessSpec& s)
    {
        AudioEffectBase::prepare(s);
        mProcessor.prepare(s);
    }

    void ChorusEffect::process(AudioBuffer<float>& buffer)
    {
        AudioBlock<float> block(buffer);
        ProcessContextReplacing<float> context(block);
        mProcessor.process(context);
    }

}
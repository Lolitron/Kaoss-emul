/*
  ==============================================================================

    PhaserEffect.cpp
    Created: 17 Dec 2021 11:44:55am
    Author:  ClarVik

  ==============================================================================
*/

#include "PhaserEffect.h"

namespace effects {

    void PhaserEffect::updateParams(int x, int y)
    {
        processor.setCentreFrequency(70+(x/100.f)*18430);
        processor.setFeedback(y/100.f);
        if (y == 0) processor.setMix(0);
        else processor.setMix(1);
    }

    void PhaserEffect::prepare(const ProcessSpec& s)
    {
        AudioEffectBase::prepare(s);
        processor.prepare(s);
        processor.setRate(0.22);
    }

    void PhaserEffect::process(AudioBuffer<float>& buffer)
    {
        AudioBlock<float> block(buffer);
        ProcessContextReplacing<float> context(block);
        processor.process(context);
    }

}
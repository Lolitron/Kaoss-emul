/*
  ==============================================================================

    Rate.h
    Created: 7 Feb 2022 2:47:57pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "Essentials.h"

namespace effects {

    class Rate : public SoundTouchEffectBase
    {
    public:

        //inherited
        virtual void updateParams(int x, int y) override;

    };

}
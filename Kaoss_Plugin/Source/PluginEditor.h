/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "../../SharedCode/timer.h"

using namespace juce;
using namespace juce::dsp;

class HandlerProcessorEditor : public juce::AudioProcessorEditor, public Timer
{
public:
    HandlerProcessorEditor(HandlerProcessor&);
    ~HandlerProcessorEditor() override;

    //==============================================================================
    void paint(juce::Graphics&) override;
    void resized() override;

    void mouseDrag(const MouseEvent& event) override;
    void mouseDown(const MouseEvent& event) override;
    void mouseUp(const MouseEvent& event) override;

    void setTouchPos(const MouseEvent& event);

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    HandlerProcessor& audioProcessor;


    TextButton resetBtn;
    std::vector<TextButton*> effectBtns;
    Rectangle<float> viewPort;
    Rectangle<float> touchPoint;
    bool mouseHeld{false};
    TextEditor bpmTextBox;
    Label bpmLabel;
    Label beatLabel;
    Label warningLabel;
    TextButton tapBpm;
    ToggleButton syncToggle;
    BubbleMessageComponent warningBubble;
    timing::Timer tapTimer;
    std::vector<int> tapPeriods;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(HandlerProcessorEditor)


        // ������������ ����� Timer
        virtual void timerCallback() override;

};


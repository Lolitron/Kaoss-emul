/*
  ==============================================================================

    AudioEffectBase.cpp
    Created: 13 Dec 2021 1:46:11pm
    Author:  ClarVik

  ==============================================================================
*/

#include "AudioEffectBase.h"

namespace effects {

	AudioEffectBase::AudioEffectBase()
	{
		startTimer(30);
	}

	void AudioEffectBase::updateParams(int x, int y)
	{
	}

	//get
	bool AudioEffectBase::isOn()
	{
		return this->mOn;
	}

	bool AudioEffectBase::isSmoothedX()
	{
		return mSmooth.first;
	}

	bool AudioEffectBase::isSmoothedY()
	{
		return mSmooth.second;
	}

	bool AudioEffectBase::isRecorded()
	{
		return mRecorded;
	}

	std::pair<int, int> AudioEffectBase::getRecordedCoords()
	{
		return mRecCoords;
	}


	//set
	void AudioEffectBase::setOn(bool on)
	{
		this->mOn = on;
	}

	void AudioEffectBase::setSmoothed(std::pair<bool, bool> smoothed)
	{
		mSmooth = smoothed;
	}

	void AudioEffectBase::setRecordedCoords(std::pair<int, int> coords)
	{
		mRecCoords = coords;
	}

	void AudioEffectBase::unrecord()
	{
		mRecorded = false;
		mRecTape.clear();
		mRecTape.shrink_to_fit();
	}

	void AudioEffectBase::setRecTape(std::vector<std::pair<int, int>> interp)
	{
		mRecTape = interp;
		mRecorded = true;
	}

	void AudioEffectBase::prepare(const ProcessSpec& s)
	{
		this->mSampleRate = s.sampleRate;
		this->mNumInputChannels = s.numChannels;
	}

	void AudioEffectBase::process(AudioBuffer<float>& buffer)
	{
	}

	void AudioEffectBase::reset()
	{
	}

	float AudioEffectBase::getZone(float n)
	{
		if (n >= .75f)
			return 4;
		else if (n >= .5f)
			return 2;
		else if (n >= .25f)
			return 1;
		else if (n >= .125f)
			return .5;
		else if (n >= .0625f)
			return .25;
		else
			return .125;
	}

	void AudioEffectBase::timerCallback()
	{
		if (mRecorded)
		{
			mRecReadPosition++;
			mRecReadPosition %= mRecTape.size();
			mRecCoords = mRecTape[mRecReadPosition];
		}
	}

	int AudioEffectBase::bpm = 200;
	bool AudioEffectBase::beat = false;

}
/*
  ==============================================================================

    Tempo.h
    Created: 16 Dec 2021 4:51:32pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "Essentials.h"

namespace effects {

    class Tempo : public SoundTouchEffectBase
    {
    public:

        //inherited
        virtual void updateParams(int x, int y) override;

    };

}
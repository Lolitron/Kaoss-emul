#include <QtGui>

#include "scribblearea.h"




 ScribbleArea::ScribbleArea(QGraphicsView *parent,UDPSender *ptr)
     : QGraphicsView(parent)
 {
     setAttribute(Qt::WA_AcceptTouchEvents);
     setAttribute(Qt::WA_StaticContents);
     sender=ptr;
     yComp=0;
     xComp=0;
     resize(901,981);
 }

 bool ScribbleArea::event(QEvent *event)
 {
     switch (event->type()) {
     case QEvent::TouchBegin:
     case QEvent::TouchUpdate:
     {
         touching=true;
         QList<QTouchEvent::TouchPoint> touchPoints = static_cast<QTouchEvent *>(event)->touchPoints();

         for(int i=0;i<rackState->size();i++)
         {
            if((*rackState)[i]->styleSheet()==btns::ButtonInfo::GreenStyle && !(*rackState)[i]->isNowRecording() && !(*rackState)[i]->isRecorded())  //needed to start recording on touch
                (*rackState)[i]->startRecord();
         }

         if(rect().contains(touchPoints[0].pos().x(),touchPoints[0].pos().y()))
         {
            if(maskTimer.elapsedMs()>=80)
            {
                yComp=(touchPoints[0].pos().y()-y())/(double)height()*100;
                xComp=(touchPoints[0].pos().x()-x())/(double)width()*100;
                QString s="p "+QString::number(xComp)+" "+QString::number(yComp);
                sender->send(s.toStdString());
                maskTimer.restart();
            }
            shapes.push_back(new CircleShape(touchPoints[0].pos().x(),touchPoints[0].pos().y()));
         }
         break;
     }
     case QEvent::TouchEnd:
     {
        touching=false;
        QString s="p "+QString::number(0)+" "+QString::number(0);
        sender->send(s.toStdString());
        xComp=0;
        yComp=0;
        break;
     }
     case QEvent::Paint:
     {
        QPainter painter(this);
        painter.setPen(Qt::transparent);

        painter.setBrush(QBrush(Qt::gray));
        for(int i=0;i<recShapes.size();i++)
            painter.drawEllipse(QPoint((recShapes[i].first/100.f)*width(),(recShapes[i].second/100.f)*height()),CircleShape::startRadius/3,CircleShape::startRadius/3);

        painter.setBrush(QBrush(CircleShape::color));
        for(int i=0;i<shapes.size();i++)
        {
            int r = ((CircleShape::lifetime-shapes[i]->getTimer()->elapsedMs())/CircleShape::lifetime)*CircleShape::startRadius;
            if(i==shapes.size()-1 && touching) // hold the last (touchUpdate is emitted only when moving so it can disappear)
                shapes[i]->getTimer()->restart();
            if(r<=0 )
                shapes.erase(shapes.begin()+i,shapes.begin()+i+1);
            else
                painter.drawEllipse(QPoint(shapes[i]->getX(),shapes[i]->getY()),r,r);

        }

        updateCorners(painter, rect().topRight());
        updateCorners(painter, rect().bottomRight());
        updateCorners(painter, rect().bottomLeft());
        updateCorners(painter, rect().topLeft());

        painter.setPen(Qt::red);
        if((*rackState)[btns::ButtonRack::btnLooper]->isChecked() ||
           (*rackState)[btns::ButtonRack::btnRevLooper]->isChecked())
        {
            painter.drawLine(x()+width()/16,0,x()+width()/16,10000);
            painter.drawLine(x()+width()/8,0,x()+width()/8,10000);
            painter.drawLine(x()+width()/4,0,x()+width()/4,10000);
            painter.drawLine(x()+width()/2,0,x()+width()/2,10000);
            painter.drawLine(x()+3*width()/4,0,x()+3*width()/4,10000);
        }

        if((*rackState)[btns::ButtonRack::btnDelay]->isChecked() ||
           (*rackState)[btns::ButtonRack::btnTapeEcho]->isChecked() ||
           (*rackState)[btns::ButtonRack::btnSingleDelay]->isChecked())
        {
            painter.drawLine(0,y()+height()/16,10000, y()+height()/16);
            painter.drawLine(0,y()+height()/8 ,10000, y()+height()/8);
            painter.drawLine(0,y()+height()/4 ,10000, y()+height()/4);
            painter.drawLine(0,y()+height()/2 ,10000, y()+height()/2);
            painter.drawLine(0,y()+3*height()/4,10000, y()+3*height()/4);
        }
        if((*rackState)[btns::ButtonRack::btnPitch]->isChecked() ||
           (*rackState)[btns::ButtonRack::btnTempo]->isChecked() ||
           (*rackState)[btns::ButtonRack::btnRate]->isChecked())
        {
            painter.drawLine(x()+width() * .45 ,0,x()+width()*.45,10000);
            painter.drawLine(x()+width() * .55 ,0,x()+width()*.55,10000);
        }
     }
     default:
         return QWidget::event(event);
     }
     return true;
 }

 void ScribbleArea::updateCorners(QPainter &painter, QPoint place)
 {
     int dx = 100 * (place == rect().topRight() || place == rect().bottomRight()?-1:1);  //determine circle deltas from corner (ternar for less space)
     int dy = 100 * (place == rect().bottomRight() || place == rect().bottomLeft()?-1:1);

     int* capture=nullptr;
     if(place == rect().topRight())
         capture=&topRightCapture;
     else if(place == rect().bottomRight())
         capture=&bottomRightCapture;
     else if(place == rect().bottomLeft())
         capture=&bottomLeftCapture;
     else if(place == rect().topLeft())
         capture=&topLeftCapture;



    gr=QRadialGradient(place.x()+dx,place.y()+dy,ScribbleArea::cornerRadius);
    gr.setColorAt(0,Qt::red);
    gr.setColorAt(1,Qt::transparent);

    if(!shapes.isEmpty())
    {
        if(touching){
            gr.setRadius((distToCenter()-
                          qSqrt(pow(shapes[shapes.size()-1]->getX()-place.x(),2)+pow(shapes[shapes.size()-1]->getY()-place.y(),2)))/
                          distToCenter()*ScribbleArea::cornerRadius);
            *capture=gr.radius();
        }
        else
            gr.setRadius(((CircleShape::lifetime-shapes[shapes.size()-1]->getTimer()->elapsedMs())/CircleShape::lifetime)*(*capture));

        if(gr.radius()<0)
            gr.setRadius(0);
    }
    else
        gr.setRadius(0);

    still=false;
    if(transientTimer.elapsedMs()-transientTimeMs > 2000 && sender->getBeat()==0)  // stale is needed to send pulse once every 2 sec after bpm button if no signal
        still=true;

    if(!touching && shapes.size()==0)
    {
        gr.setRadius(cornerRadius/1.3);

        if(sender->getBeat() == 0)
        {
            if(transientTimeMs-transientTimer.elapsedMs()<0)
            {
                gr.setColorAt(0,QColor(255,0,0, 100));
            }
            else
            {
                gr.setColorAt(0,QColor(255,0,0, 100 + 100 * ((transientTimeMs-transientTimer.elapsedMs())/(double)transientTimeMs)));
            }
        }
        else if(sender->getBeat() != 0)
        {
            gr.setColorAt(0,QColor(255,0,0, 100 + 100 * (sender->getBeat()/100.)));
            if(!still) transientTimer.restart();
        }
    }
    else
    {
        gr.setColorAt(0,Qt::red);
    }


    painter.fillRect(rect().topLeft().x(),rect().topLeft().y(),1000,1000,gr);

 }

 int ScribbleArea::distToCenter()
 {
     return qSqrt(pow(rect().center().x()-rect().topLeft().x(),2)+pow(rect().center().y()-rect().topLeft().y(),2));
 }


 QString ScribbleArea::getAxis() {
     return QString::number(xComp)+" "+QString::number(yComp);
 }


/*
  ==============================================================================

    PhaserEffect.h
    Created: 17 Dec 2021 11:44:55am
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "Essentials.h"

namespace effects {

    class PhaserEffect : public AudioEffectBase
    {
    public:
        void updateParams(int x, int y) override;
        void prepare(const ProcessSpec& s) override;
        void process(AudioBuffer<float>& buffer) override;

    private:

        Phaser<float> processor;
    };

}
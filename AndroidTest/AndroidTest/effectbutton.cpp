#include <QtGui>

#include "effectbutton.h"

EffectButton::EffectButton(QRect geometry, std::string text, QWidget* parent, int effNum):
    QPushButton(QString(text.c_str()))
{
    setGeometry(geometry);
    setAttribute(Qt::WA_AcceptTouchEvents);
    effectNumber = effNum;

    setParent(parent);
    effect = new QGraphicsDropShadowEffect(this);
    effect->setColor(Qt::red);
    effect->setOffset(0);
    effect->setBlurRadius(btns::ButtonInfo::GlowRadius);
    effect->setEnabled(false); //off by default (all buttons off)
    effect->connect(this, &QPushButton::toggled, this, [=] () {  //lambda to toggle shadow as glow
        this->effect->setEnabled(!effect->isEnabled());
    });
    setGraphicsEffect(effect);


    QFont font("MS Shell Dlg 2",13);
    font.setBold(true);
    setFont(font);

    resize(geometry.width(), geometry.height());

    setCheckable(true);

    setStyleSheet(btns::ButtonInfo::RedStyle);

    recorded = QVector<QPair<int,int>>();
}

bool EffectButton::event(QEvent *event)
{
    switch (event->type())
        {
        case QEvent::TouchBegin:
        {
            holdtimer.restart();
            break;
        }
        case QEvent::TouchEnd:
        {
            setChecked(!isChecked());
            if(holdtimer.elapsedMs() >= holdToRecordMs)
            {
                setChecked(true);
                if(styleSheet()==btns::ButtonInfo::GreenStyle)
                {
                    unrecord();
                    setStyleSheet(btns::ButtonInfo::RedStyle);
                    effect->setColor(Qt::red);
                    emit unrecorded();
                }
                else
                {
                    setStyleSheet(btns::ButtonInfo::GreenStyle);
                    effect->setColor(Qt::green);
                }
            }
            break;
        }
        default:
            return QPushButton::event(event);
        }

    return true;

}

QPair<int,int> EffectButton::getNextPair()
{
    recReadPosition+=1;
    recReadPosition%=recorded.size();
    return recorded[recReadPosition];
}

std::string EffectButton::createRow()
{
    std::string res = "r "+std::to_string(effectNumber)+" ";
    for(int i=0;i<recorded.size();i++)
        res+=std::to_string(recorded[i].first)+" "+std::to_string(recorded[i].second)+" ";
    return res;
}

bool EffectButton::isRecorded()
{
    return recorded.size()!=0 && !nowRecording;
}

void EffectButton::addPair(int x, int y)
{
    recorded.append(QPair<int,int>(x,y));
}

void EffectButton::stopRecord()
{
    nowRecording = false;
}

void EffectButton::startRecord()
{
    nowRecording=true;
    emit recordStarted();
}


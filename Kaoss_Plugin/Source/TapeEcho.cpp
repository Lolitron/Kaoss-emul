/*
  ==============================================================================

    TapeEcho.cpp
    Created: 16 Jan 2022 9:38:57pm
    Author:  ClarVik

  ==============================================================================
*/

#include "TapeEcho.h"

namespace effects {

    void TapeEcho::prepare(const ProcessSpec& s)
    {
        Delay::prepare(s);
        mFilter.prepare(s);
        mFilter.setType(StateVariableTPTFilterType::lowpass);
        mFilter.setCutoffFrequency(800);
    }

    void TapeEcho::process(AudioBuffer<float>& buffer)
    {

        if (!mShouldProcess)
        {
            delayBuffer.clear();
            return;
        }

        int readPointer, writePointer;

        for (int channel = 0; channel < mNumInputChannels; channel++)
        {
            mpDryBufferData = buffer.getWritePointer(channel);
            mpDelayBufferData = delayBuffer.getWritePointer(channel);
            readPointer = mReadPosition;
            writePointer = mWritePosition;

            for (int i = 0; i < buffer.getNumSamples(); i++)
            {
                const float in = mpDryBufferData[i];
                float out = 0.0;

                out = in + mpDelayBufferData[readPointer] * mFeedback;

                mpDelayBufferData[writePointer] = mFilter.processSample(channel, out);

                if (++readPointer >= delayBuffer.getNumSamples())
                    readPointer = 0;
                if (++writePointer >= delayBuffer.getNumSamples())
                    writePointer = 0;

                mpDryBufferData[i] = out;
            }

        }

        mWritePosition = writePointer;
        mReadPosition = (int)(mWritePosition - (mDelayFreq / 1000. * mSampleRate) + delayBuffer.getNumSamples()) % delayBuffer.getNumSamples();

    }

}
/*
  ==============================================================================

    Pitch.cpp
    Created: 15 Dec 2021 8:55:31pm
    Author:  ClarVik

  ==============================================================================
*/

#include "Pitch.h"

namespace effects {

    void Pitch::updateParams(int x, int y)
    {
        if (y == 0 ) {
            if (!pProcessor1->isEmpty() && !pitcherCleared)
            {
                pProcessor1->clear();
                pProcessor2->clear();
                pitcherCleared = true;
            }
            pProcessor1->setPitchSemiTones(0);
            pProcessor2->setPitchSemiTones(0);
        }
        else {
            pitcherCleared = false;
            pProcessor1->setPitchSemiTones((float)(-12 + getZone(x / 100.) * 24));
            pProcessor2->setPitchSemiTones((float)(-12 + getZone(x / 100.) * 24));
        }

        mShouldProcess = y != 0;
    }

}
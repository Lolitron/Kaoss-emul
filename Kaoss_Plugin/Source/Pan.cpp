/*
  ==============================================================================

    Pan.cpp
    Created: 29 Dec 2021 8:56:35pm
    Author:  ClarVik

  ==============================================================================
*/

#include "Pan.h"

namespace effects {

    void Pan::updateParams(int x, int y)
    {
        if (y == 0) processor.setPan(0);
        else
            processor.setPan(-1 + (x / 100.f) * 2);
    }

    void Pan::prepare(const ProcessSpec& s)
    {
        processor.prepare(s);
    }

    void Pan::process(AudioBuffer<float>& buffer)
    {
        AudioBlock<float> block(buffer);
        ProcessContextReplacing<float> context(block);
        processor.process(context);
    }

}

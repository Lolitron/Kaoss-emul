/*
  ==============================================================================

    HPF.h
    Created: 17 Dec 2021 12:22:40pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "FilterBase.h"

namespace effects {

    class HPF : public FilterBase
    {
    public:
        void updateParams(int x, int y) override;
        void prepare(const ProcessSpec& s) override;
    };

}
#ifndef UDPCLIENT_H
#define UDPCLIENT_H


#include <QObject>
#include <QUdpSocket>
#include <QWidget>
#include <stdio.h>
#include <iostream>
#include <cstring>
#include "../../SharedCode/globals.h"
#include "../../SharedCode/Timer.h"




using namespace std;

class UDPSender : public QWidget
{
    Q_OBJECT
public:
    UDPSender(QWidget *parent = nullptr);

public slots:

    void processPendingDatagrams();

signals:
    void refresh();

public:

    void send(string data);

    int getBeat() {return beat;}
    void setBeat(int b) {beat = b;}

    bool getPeak(){return peak;}

private:
    int socketHandle;
    char* data;
    QUdpSocket* socket;
    int beat=0;
    bool peak = false;
    timing::Timer latencyTimer;
};

#endif // UDPCLIENT_H

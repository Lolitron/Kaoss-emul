#ifndef TIMER_H
#define TIMER_H

#include <ctime>

namespace timing {

    class Timer
    {
    private:

        double start;

    public:
        Timer()
        {
            start=clock();
        }

        Timer(double start)
        {
            this->start=start;
        }

        void restart()
        {
            start=clock();
        }

        //return elapsed time in ms
        double elapsedMs()
        {
            return (static_cast<double>(clock() - start) / CLOCKS_PER_SEC) * 1000.;
        }
    };


}
#endif // TIMER_H

/*
  ==============================================================================

    BPF.cpp
    Created: 18 Dec 2021 6:13:42pm
    Author:  ClarVik

  ==============================================================================
*/

#include "BPF.h"

namespace effects {

    void BPF::updateParams(int x, int y)
    {
        filter.setCutoffFrequency(1. + (x / 100.) * 10000.);
        mShouldProcess = y != 0;
        mDryWet.setWetMixProportion(y / 100.);
    }

    void BPF::prepare(const ProcessSpec& s)
    {
        AudioEffectBase::prepare(s);
        filter.setType(StateVariableTPTFilterType::bandpass);
        filter.prepare(s);
        mDryWet.prepare(s);
    }

    void BPF::process(AudioBuffer<float>& buffer)
    {
        AudioBlock<float> block(buffer);
        ProcessContextReplacing<float> context(block);
        if (mShouldProcess) {
            mDryWet.pushDrySamples(block);
            filter.process(context);
            mDryWet.mixWetSamples(block);
        }
    }

}
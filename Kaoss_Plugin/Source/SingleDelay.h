/*
  ==============================================================================

    SingleDelay.h
    Created: 30 Dec 2021 4:38:20pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "Delay.h"

namespace effects {

    class SingleDelay : public Delay
    {
    public:
        void updateParams(int x, int y) override;
        void process(AudioBuffer<float>& buffer) override;

    private:

        Panner<float> mPanner;
    };

}
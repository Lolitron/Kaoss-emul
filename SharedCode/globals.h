#ifndef BUTTONRACK_H
#define BUTTONRACK_H

#include <string>

#define PORT 27015
#define MAXLINE 2048

namespace btns {


enum ButtonRack
{
    btnRate,
    btnTempo,
    btnPitch,
    btnDelay,
    btnSingleDelay,
    btnTapeEcho,
    btnLooper,
    btnRevLooper,
    btnChorus,
    btnDecimator,
    btnFlanger,
    btnReverb,
    btnCompressor,
    btnPan,
    btnHPF,
    btnLPF,
    btnBPF,
    numEffects //it will automatically be the number of defined names as they increment from zero
};

static std::string toString(ButtonRack el)
{
    switch(el)
    {
    case btnLooper:
        return "LOOPER";
    case btnRevLooper:
        return "REVLOOP";
    case btnDelay:
        return "DELAY";
    case btnSingleDelay:
        return "1DELAY";
    case btnTapeEcho:
        return "TAPE";
    case btnReverb:
        return "REVERB";
    case btnChorus:
        return "CHORUS";
    case btnDecimator:
        return "DECIM";
    case btnFlanger:
        return "FLANGER";
    case btnPan:
        return "PAN";
    case btnCompressor:
        return "COMP";
    case btnHPF:
        return "HPF";
    case btnLPF:
        return "LPF";
    case btnBPF:
        return "BPF";
    case btnTempo:
        return "TEMPO";
    case btnPitch:
        return "PITCH";
    case btnRate:
        return "RATE";
    }
}


struct ButtonInfo
{
public:
    static constexpr auto GreenStyle = "\
            QPushButton:checked{\
            border: 10px solid green;\
            color: green;\
        }\
            QPushButton{\
            border: 10px solid gray;\
            border-radius: 23px;\
            color: gray;\
        }";



    static constexpr auto RedStyle = "\
            QPushButton:checked{\
            border: 10px solid red;\
            color: red;\
        }\
            QPushButton{\
            border: 10px solid gray;\
            border-radius: 23px;\
            color: gray;\
        }";

    static const int ButtonSize = 183;
    static const int ButtonOffset = 25;
    static const int GlowRadius = 120;
};


}


#endif // BUTTONRACK_H

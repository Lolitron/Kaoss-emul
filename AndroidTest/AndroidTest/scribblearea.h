#ifndef SCRIBBLEAREA_H
#define SCRIBBLEAREA_H

#include <QColor>
#include <QGraphicsView>
#include <QImage>
#include <QPoint>
#include <QTouchEvent>
#include <QWidget>
#include "udpsender.h"
#include "circleshape.h"
#include "../../SharedCode/timer.h"
#include "effectbutton.h"
#include "../../SharedCode/globals.h"



class ScribbleArea : public QGraphicsView
{
    Q_OBJECT

public:
    ScribbleArea(QGraphicsView *parent = 0,UDPSender* sender=0);

    //get formated string "{x} {y}"
    QString getAxis();
    bool isStill(){return still;}
    QPair<int,int> getComps(){return QPair<int,int>(xComp,yComp);}

    void setRackState(QVector<EffectButton*>* rackPtr) {rackState = rackPtr;}
    void drawCircle(int x, int y) {recShapes.append(QPair<int,int>(x,y));};
    void clearRec() {recShapes.clear();};

protected:

    bool event(QEvent *event);

    void updateCorners(QPainter &painter, QPoint place);

private:
    UDPSender *sender;
    QVector<CircleShape*> shapes;
    int xComp;
    int yComp;
    timing::Timer maskTimer; //timer to send signal every period? to avoid latency
    bool touching=false;
    static const int cornerRadius=350;

    QRadialGradient gr;
    int topLeftCapture,topRightCapture,bottomRightCapture,bottomLeftCapture;
    timing::Timer transientTimer; //corner transient timer
    bool still=false;

    QVector<EffectButton*>* rackState; //to check what buttons are checked

    QVector<QPair<int,int>> recShapes;


    static const int transientTimeMs = 500;

    int distToCenter();

};

#endif

/*
  ==============================================================================

    DelayCustom.cpp
    Created: 19 Dec 2021 12:44:37pm
    Author:  ClarVik

  ==============================================================================
*/

#include "Delay.h"

namespace effects {

    void Delay::updateParams(int x, int y)
    {
        mDelayFreq = getZone(y/100.f) * (60./bpm) * 1000; 
        mFeedback = (x / 100.f) * 0.9;
        mShouldProcess = y != 0;
    }

    void Delay::prepare(const ProcessSpec& s)
    {
        AudioEffectBase::prepare(s);
        const int delayBufferSize = (s.sampleRate + s.maximumBlockSize) * 2.;
        delayBuffer.setSize(s.numChannels, delayBufferSize);
        delayBuffer.clear();

        mReadPosition = (int)(mWritePosition - (mDelayFreq/1000. * mSampleRate) + delayBufferSize) % delayBufferSize;
    }

    void Delay::process(AudioBuffer<float>& buffer)
    {
        if (!mShouldProcess)
        {
            delayBuffer.clear();
            return;
        }

        int readPointer, writePointer;

        for (int channel = 0; channel < mNumInputChannels; channel++)
        {
            mpDryBufferData = buffer.getWritePointer(channel);
            mpDelayBufferData = delayBuffer.getWritePointer(channel);
            readPointer = mReadPosition;
            writePointer = mWritePosition;

            for (int i = 0; i < buffer.getNumSamples(); i++)
            {
                const float in = mpDryBufferData[i];
                float out = 0.0;

                out = in + mpDelayBufferData[readPointer];

                mpDelayBufferData[writePointer] = out * mFeedback;

                if (++readPointer >= delayBuffer.getNumSamples())
                    readPointer = 0;
                if (++writePointer >= delayBuffer.getNumSamples())
                    writePointer = 0;

                mpDryBufferData[i] = out;
            }

        }

        mWritePosition = writePointer;
        mReadPosition = (int)(mWritePosition - (mDelayFreq / 1000. * mSampleRate) + delayBuffer.getNumSamples()) % delayBuffer.getNumSamples();


        /*for (int channel = 0; channel < buffer.getNumChannels(); channel++)
        {
            mpBufferData = buffer.getReadPointer(channel);
            mpDelayBufferData = delayBuffer.getReadPointer(channel);
            mpDryBufferData = buffer.getWritePointer(channel);

            fillDelayBuffer(channel, buffer.getNumSamples(), delayBuffer.getNumSamples(), mpBufferData);
            if (mFeedback != 0) getFromDelayBuffer(buffer, channel, buffer.getNumSamples(), delayBuffer.getNumSamples(), mpDelayBufferData);
            feedbackDelay(channel, buffer.getNumSamples(), delayBuffer.getNumSamples(), mpDryBufferData);
        }

        mWritePosition += buffer.getNumSamples();
        mWritePosition %= delayBuffer.getNumSamples();*/

    }


    ////put samples from main buffer to delayBuffer (wrapping around zero if refill)

    //void Delay::fillDelayBuffer(int channel, const int bufferLength, const int delayBufferLength,
    //    const float* bufferData)
    //{
    //    if (delayBufferLength > mWritePosition + bufferLength) //we have enough space in delayBuffer to add next block to it 
    //    {
    //        delayBuffer.copyFromWithRamp(channel, mWritePosition, bufferData, bufferLength, 1.f, 1.f);
    //    }
    //    else
    //    {
    //        const int bufferRemaining = delayBufferLength - mWritePosition;

    //        delayBuffer.copyFromWithRamp(channel, mWritePosition, bufferData, bufferRemaining, 1.f, 1.f);  //add piece of block that fits
    //        delayBuffer.copyFromWithRamp(channel, 0, bufferData, bufferLength - bufferRemaining, 1.f, 1.f);  //wrap around zero and add the rest
    //    }
    //}

    //void Delay::getFromDelayBuffer(juce::AudioBuffer<float>& buffer, int channel, const int bufferLength, const int delayBufferLength,
    //    const float* delayBufferData)
    //{
    //    const int readPosition = static_cast<int>(delayBufferLength + mWritePosition - (mSampleRate * mDelayFreq / 1000.)) % delayBufferLength;  //position where we are writing minus delayOffset(conv to ms) in samples

    //    if (delayBufferLength > readPosition + bufferLength)  //if we have enough space
    //    {
    //        buffer.addFromWithRamp(channel, 0, delayBufferData + readPosition, bufferLength, 1  * mShouldProcess, 1  * mShouldProcess); //insert segment in main buffer
    //    }
    //    else
    //    {
    //        const int bufferRemaining = delayBufferLength - readPosition;
    //        buffer.addFromWithRamp(channel, 0, delayBufferData + readPosition, bufferRemaining, 1   * mShouldProcess, 1   * mShouldProcess);   //insert a piece
    //        buffer.addFromWithRamp(channel, bufferRemaining, delayBufferData, bufferLength - bufferRemaining, 1   * mShouldProcess, 1   * mShouldProcess);  //wrap and add remaining from the segment start
    //    }
    //}

    //void Delay::feedbackDelay(int channel, const int bufferLength, const int delayBufferLength, float* dryBuffer)
    //{
    //    if (delayBufferLength > bufferLength + mWritePosition)
    //    {
    //        delayBuffer.addFromWithRamp(channel, mWritePosition, dryBuffer, bufferLength, mFeedback, mFeedback);
    //    }
    //    else
    //    {
    //        const int bufferRemaining = delayBufferLength - mWritePosition;

    //        delayBuffer.addFromWithRamp(channel, bufferRemaining, dryBuffer, bufferRemaining, mFeedback, mFeedback);
    //        delayBuffer.addFromWithRamp(channel, 0, dryBuffer, bufferLength - bufferRemaining, mFeedback, mFeedback);
    //    }
    //}

}
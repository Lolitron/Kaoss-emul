/*
  ==============================================================================

    AudioEffectBase.h
    Created: 13 Dec 2021 1:46:11pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>
#include "vector"
#include "algorithm"

using namespace juce;
using namespace juce::dsp;

namespace effects {

	class AudioEffectBase : private juce::Timer
	{
	public:

		AudioEffectBase();

		virtual void updateParams(int x, int y);

		//get
		bool isOn();
		bool isSmoothedX();
		bool isSmoothedY();
		bool isRecorded();
		std::pair<int, int> getRecordedCoords();

		//set
		void setOn(bool on);
		void setSmoothed(std::pair<bool, bool> smoothed);
		void setRecordedCoords(std::pair<int, int> coords);
		void unrecord();
		void setRecTape(std::vector<std::pair<int, int>> interp);


		virtual void prepare(const ProcessSpec& s);
		virtual void process(AudioBuffer<float>& buffer);
		virtual void reset();


		static int bpm;
		static bool beat;

	protected:
		virtual float getZone(float n);

		bool mOn{ false };
		bool mShouldProcess{ true };
		float mSampleRate{ 44100 };
		int mNumInputChannels{ 1 };
		std::pair<bool, bool> mSmooth{ std::make_pair(true,true) };  //x, y

		bool mRecorded{ false };
		std::pair<int, int> mRecCoords;
		std::vector<std::pair<int, int>> mRecTape;
		int mRecReadPosition{-1};

		// ������������ ����� Timer
		void timerCallback() override;
	};

}
/*
  ==============================================================================

    DelayCustom.h
    Created: 19 Dec 2021 12:44:37pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "Essentials.h"


class CustomDelay : public AudioEffectBase
{
public:
    void updateParams(int x, int y) override;
    void prepare(const ProcessSpec& s) override;
    void process(AudioBuffer<float>& buffer) override;

private:

    void fillDelayBuffer(int channel, const int bufferLength, const int delayBufferLength, const float* bufferData);
    void getFromDelayBuffer(juce::AudioBuffer<float>& buffer, int channel, const int bufferLength, const int delayBufferLength, const float* delayBufferData);
    void feedbackDelay(int channel, const int bufferLength, const int delayBufferLength, float* dryBuffer);

    AudioBuffer<float> mDelayBuffer;
    int mDelayFreq{200};
    float mFeedback{0.5f};
    int mWritePosition{ 0 };
};
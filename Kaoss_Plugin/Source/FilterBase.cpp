/*
  ==============================================================================

    FilterBase.cpp
    Created: 17 Dec 2021 12:18:21pm
    Author:  ClarVik

  ==============================================================================
*/

#include "FilterBase.h"

namespace effects {

    void FilterBase::process(AudioBuffer<float>& buffer)
    {
        if (!mShouldProcess) return;
        AudioBlock<float> block(buffer);
        ProcessContextReplacing<float> context(block);
        filter.process(context);
    }

}

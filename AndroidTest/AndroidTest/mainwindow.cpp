#include "mainwindow.h"
#include "ui_mainwindow.h"
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include "udpsender.h"

#include <QStyle>

#pragma comment(lib, "Ws2_32.lib")


struct Effect{
    int number;
    int dryWet;
    int feature;

    Effect(int n,int dw,int f)
    {
        number=n;
        dryWet=dw;
        feature=f;
    }
};

struct GroupData : QObjectUserData
{
    int numChecked;

    GroupData(int n)
    {
        numChecked=n;
    }
};

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    sender = new UDPSender(this);
    area=new ScribbleArea(ui->pane,sender);
    ui->pane->setAttribute(Qt::WA_AcceptTouchEvents);

    ui->btnRec->setVisible(false);

    timerRecordBar = new QTimer();
    connect(timerRecordBar,SIGNAL(timeout()),this,SLOT(on_RecordBarTick()));
    timerRecordTick = new QTimer();
    connect(timerRecordTick,SIGNAL(timeout()),this,SLOT(on_RecordTick()));

    int buttonSize = btns::ButtonInfo::ButtonSize;

    buttonOffset = (this->width()-(buttonSize*(this->width()/buttonSize)))/((this->width()/buttonSize)+1);

    ui->btnBPM->setGeometry(buttonOffset,
                            ui->line2->y() + (ui->line1->y()-ui->line2->y()-btns::ButtonInfo::ButtonSize)/2 + 10 ,//border thickness
                            btns::ButtonInfo::ButtonSize,btns::ButtonInfo::ButtonSize);
    //buttonOffset = btns::ButtonInfo::ButtonOffset;
    EffectButton* temporal;
    for(int i=0, j=0, c=0;c<numEffects;i++, c++)
    {
        temporal = new EffectButton(
                    QRect(buttonOffset+(buttonSize+buttonOffset)*i,
                          ui->progressBar->height()+buttonOffset*1.2+(buttonSize+buttonOffset)*j,
                          buttonSize,
                          buttonSize),
                    btns::toString((btns::ButtonRack)c), this, c);


        buttonRack.push_back(temporal);
        this->layout()->addWidget(temporal);
        temporal->setAttribute(Qt::WA_AcceptTouchEvents);

        connect(temporal, &QPushButton::toggled, this, [=]()
        {
            this->sender->send("e " + std::to_string(c)+" "+std::to_string((int)temporal->isChecked()));
        });

        connect(temporal, &EffectButton::recordStarted, this, [=]()
        {
            timerRecordBar->start(4 * (60000.f/ui->sbBPM->value()));
        });

        connect(temporal, &EffectButton::unrecorded, this, [=]()
        {
            this->sender->send("ur " + std::to_string(c));
        });

        if(i+1>this->width()/(buttonSize+buttonOffset*2)) // if out of bounds
        {
            j++;
            i=-1; //will increment
        }

    }
    timerRecordTick->start(80);


    int w = ui->progressBar->width()/ui->progressBar->maximum();
    ui->progressBar->setStyleSheet("QProgressBar:horizontal {\
                                   background: black;\
                                   border: 2px solid grey;\
                                   border-radius: 10px;\
                                   }\
                                   \
                                   QProgressBar::chunk:horizontal {\
                                   background: #9B0000;\
                                   margin: 10px; \
                                   border-radius: 3px;\
                                   width: "+QString::number(w-20)+";\
                                   }");

    periods = QQueue<int>();
    timer=new timing::Timer();

    repaintTimer=new QTimer();
    repaintTimer->start(10);
    connect(repaintTimer,SIGNAL(timeout()),area,SLOT(repaint()));

    timerBPMFlash=new QTimer();
    timerBPMFlash->start(60.0/ui->sbBPM->value()*1000);
    connect(timerBPMFlash,SIGNAL(timeout()),this,SLOT(flashBPMButton()));
    timerEveryMs=new QTimer();
    timerEveryMs->start(1);
    connect(timerEveryMs,SIGNAL(timeout()),this,SLOT(everyMs()));


    area->setRackState(&buttonRack);

    ui->verticalSlider->setAttribute(Qt::WA_AcceptTouchEvents);

    connect(sender, SIGNAL(refresh()),this, SLOT(sendAllStates()));
}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_btnBPM_pressed()
{
    periods.push_back(timer->elapsedMs());
    if(periods.size()>6)
        periods.pop_front();

    double sum=0.;
    for(int period : periods)
        sum+=period;
    sum/=periods.size();

    ui->sbBPM->setValue((int)((10000*periods.size()/sum)));
    timer->restart();
}


void MainWindow::on_sbBPM_valueChanged(int arg1)
{
    //if(ui->sbBPM->value()<120) ui->sbBPM->setValue(ui->sbBPM->value()*2);
    sender->send("bpm "+to_string(ui->sbBPM->value()));
    timerBPMFlash->start(60.0/ui->sbBPM->value()*1000);
}

void MainWindow::on_verticalSlider_valueChanged(int value)
{
    sender->send("v "+to_string(value));
}

//timer callback
void MainWindow::flashBPMButton()
{
    ui->btnBPM->setStyleSheet("QPushButton{\n                     border: 10px solid red;\n                     border-radius: 23px;\n					color: red;\n                }");
    msFlash=0;
    if(area->isStill())
        sender->setBeat(30);
}

void MainWindow::everyMs()
{
    if(msFlash==3)
    {
        msFlash=0;
        ui->btnBPM->setStyleSheet("QPushButton{\n                     border: 10px solid gray;\n                     border-radius: 23px;\n					color: gray;\n                }");
        if(area->isStill())
            sender->setBeat(0);
    }
    else
        msFlash++;

    ui->peakMeter->setChecked(sender->getPeak());
}


void MainWindow::sendAllStates()
{
    for(int i=0;i<buttonRack.size();i++){
        this->sender->send("ur "+std::to_string(i));
        this->sender->send("e " + std::to_string(i)+" "+std::to_string((int)buttonRack[i]->isChecked()));
        if(buttonRack[i]->isRecorded())
            this->sender->send(buttonRack[i]->createRow());
    }
    sender->send("v "+to_string(ui->verticalSlider->value()));
    sender->send("bpm "+to_string(ui->sbBPM->value()));
}


void MainWindow::on_btnRec_clicked()
{
    if(ui->btnRec->isChecked())
    {
        for(int i=0;i<buttonRack.size();i++)
        {
            if(buttonRack[i]->isChecked())
            {
                buttonRack[i]->unrecord();
                buttonRack[i]->setStyleSheet("QPushButton{\n                     border: 10px solid green;\n                     border-radius: 23px;\n					color: green;\n                }");
            }
        }
    }

}

void MainWindow::on_RecordBarTick()
{
    if(ui->progressBar->value()==ui->progressBar->maximum()-1)
    {
        timerRecordBar->stop();
        ui->progressBar->setValue(0);
        for(int i=0;i<buttonRack.size();i++)
        {
            if(buttonRack[i]->isNowRecording())
            {
                buttonRack[i]->stopRecord();
                this->sender->send(buttonRack[i]->createRow());
            }
        }
    }
    else
    {
        ui->progressBar->setValue(ui->progressBar->value()+1);
    }
}

void MainWindow::on_RecordTick()
{
    area->clearRec();
    for(int i=0;i<buttonRack.size();i++)
    {
        if(buttonRack[i]->isNowRecording())
            buttonRack[i]->addPair(area->getComps().first,area->getComps().second);
        if(buttonRack[i]->isRecorded()){
            QPair<int,int> coords = buttonRack[i]->getNextPair();//do not stop cycle through recorded
            if(buttonRack[i]->isChecked())
            {
                area->drawCircle(coords.first,coords.second);
//                sender->send("r "+std::to_string(i)+" "+std::to_string(coords.first)+" "+std::to_string(coords.second));
            }
        }
    }
}


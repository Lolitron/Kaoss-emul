/*
  ==============================================================================

    UDPServer.h
    Created: 21 Jan 2022 4:22:44pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <windows.h>
#include <string>
#include <thread>
#include <ws2tcpip.h>
#include "PluginProcessor.h"
#include "../../SharedCode/globals.h"



class UDPServer
{
public:

    UDPServer(HandlerProcessor *p)
    {
        this->parent = p;
        data = (char*)malloc(MAXLINE);
        

        if(WSAStartup(MAKEWORD(2, 2), &wsaData)!=0)
            MessageBox(0, std::to_string(result).c_str(), "Error startup : ", 1);

        addr.sin_family = AF_INET;
        addr.sin_port = htons(PORT);
        addr.sin_addr.s_addr = INADDR_ANY;


        listenSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        if (listenSocket == INVALID_SOCKET)
            MessageBox(0, std::to_string(WSAGetLastError()).c_str(), "Error creating: ", 1);
        
        int optval = 1;
        setsockopt(listenSocket, SOL_SOCKET, SO_REUSEADDR, (const char*)&optval, sizeof optval);

        if (bind(listenSocket, (sockaddr*)&addr, sizeof(addr)) == SOCKET_ERROR) {
            MessageBox(0, std::to_string(WSAGetLastError()).c_str(), "Error binding: ", 1);
        }

        buffer.len = MAXLINE;
        buffer.buf = data;

        SecureZeroMemory((PVOID)&overlapped, sizeof(WSAOVERLAPPED));
        overlapped.hEvent = WSACreateEvent();

    }

    void recieveAsync()
    {
        std::thread th(&UDPServer::recieve, this);
        th.join();
    }

    void sendBeatAsync()
    {
        std::thread th(&UDPServer::sendBeat, this);
        th.join();
    }

    void sendPeakAsync()
    {
        std::thread th(&UDPServer::sendPeak, this);
        th.join();
    }

    void askRefreshAsync()
    {
        std::thread th(&UDPServer::askRefresh, this);
        th.join();
    }

    ~UDPServer() 
    {
        closesocket(listenSocket);
        WSACleanup();
    }

private:

    std::vector<std::string> split(std::string data)
    {
        std::istringstream iss(data);
        std::vector<std::string> results(std::istream_iterator<std::string>{iss}, std::istream_iterator<std::string>());
        return results;
    }

    void sendBeat()
    {
        strcpy(data, ("beat " + std::to_string((int)parent->getBeat()) + " ").c_str());
        result = WSASendTo(listenSocket, &buffer, 1, &operatedBytes, 0, (sockaddr*)&senderaddr, dataLen, &overlapped, NULL);
    }

    void sendPeak()
    {
        strcpy(data, ("peak " + std::to_string((int)parent->getPeak()) + " ").c_str());
        result = WSASendTo(listenSocket, &buffer, 1, &operatedBytes, 0, (sockaddr*)&senderaddr, dataLen, &overlapped, NULL);
    }

    void askRefresh() 
    {
        strcpy(data, "ask ");
        result = WSASendTo(listenSocket, &buffer, 1, &operatedBytes, 0, (sockaddr*)&senderaddr, dataLen, &overlapped, NULL);
    } 
    
    void returnTime() 
    {
        strcpy(data, "time ");
        result = WSASendTo(listenSocket, &buffer, 1, &operatedBytes, 0, (sockaddr*)&senderaddr, dataLen, &overlapped, NULL);
    }

    void recieve()
    {

        ZeroMemory(data, MAXLINE);
        dataLen = sizeof(senderaddr);

        DWORD Flags = 0;
        result = WSARecvFrom(listenSocket, &buffer, 1, &operatedBytes, &Flags, (sockaddr*)&senderaddr, &dataLen, &overlapped, NULL);

        mSplitted = split(data);
        if (mSplitted.size() == 0)
            return;


        if (mSplitted[0] == "p") {
            parent->x.setTargetValue(atoi(mSplitted[1].c_str()));
            parent->y.setTargetValue(atoi(mSplitted[2].c_str()));
        }
        else if (mSplitted[0] == "e")
        {
            parent->setOn(atoi(mSplitted[1].c_str()), atoi(mSplitted[2].c_str()));
        }
        else if (mSplitted[0] == "v")
        {
            parent->setGain(atof(mSplitted[1].c_str())/100.f);;
        }
        else if (mSplitted[0] == "bpm")
        {
            parent->applyNewBpm(atoi(mSplitted[1].c_str()));
        }
        else if (mSplitted[0] == "r")
        {
            //parent->setRecordedCoords(atoi(splitted[1].c_str()),std::make_pair(atoi(splitted[2].c_str()),atoi(splitted[3].c_str())));
            std::vector<std::pair<int, int>> res;
            for (int i = 2; i+1 < mSplitted.size(); i+=2)
                res.push_back(std::make_pair(atoi(mSplitted[i].c_str()),atoi(mSplitted[i+1].c_str())));
            parent->record(atoi(mSplitted[1].c_str()),res);
        }
        else if (mSplitted[0] == "ur")
        {
            parent->unrecord(atoi(mSplitted[1].c_str()));
        }
        else if (mSplitted[0] == "time") {
            double lat = timing::Timer(atof(mSplitted[1].c_str())).elapsedMs();
            std::thread th(&UDPServer::returnTime, this);
            th.join();
            std::cout << "awd";
        }

    }

    WSADATA wsaData;
    SOCKET listenSocket = INVALID_SOCKET;
    char* data;
    std::vector<std::string> mSplitted;
    sockaddr_in addr;
    sockaddr_in senderaddr;
    int dataLen;
    WSABUF buffer;
    DWORD operatedBytes; // num bytes sent/recieved
    WSAOVERLAPPED overlapped;
    int result;
    bool nowRecieving = false;

    bool beat = false;
    HandlerProcessor* parent;

};
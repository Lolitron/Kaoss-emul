/*
  ==============================================================================

    BeatDetector.cpp
    Created: 8 Jan 2022 11:47:21pm
    Author:  ClarVik

  ==============================================================================
*/

#include "BeatDetector.h"
#include "PluginProcessor.h"
#include "PluginEditor.h"


BeatDetector::BeatDetector(HandlerProcessor* p) : Thread("BeatDetector Thread"), parent(p)
{
    blocksPerSecond = round(parent->getSampleRate() / 1024);
    kickMin = round(50 / blocksPerSecond); //Hz/bandSize to access fftData
    kickMax = round(150 / blocksPerSecond); 
    snareMin = round(700 / blocksPerSecond); 
    snareMax = round(2300 / blocksPerSecond); 
    bandKick = (kickMax - kickMin+1) * 2;  //formula (1/n) + channels
    bandSnare = (snareMax - snareMin+1) * 2;
}

void BeatDetector::run()
{
    beatDetection();
}

void BeatDetector::beatDetection()
{
    std::vector<float> energyRange;

    energyRange.push_back(performEnergySum(0));
    energyRange.push_back(performEnergySum(1));


    energyHistory.push(energyRange);

    if (energyHistory.size() > 0)
    {
        calculateThreshold();
        
        if (energyRange[SpectrumBand::kick] - 0.1 > threshold[SpectrumBand::kick] * average[0] )
            parent->setBeat(100);
        //if(energyRange[SpectrumBand::snare] - 0.005 > threshold[SpectrumBand::snare] * average[1])
        //   parent->setBeat(50);
        else
            parent->setBeat(0);


        if(energyHistory.size()>blocksPerSecond)
            energyHistory.pop();
    }
}

float BeatDetector::performEnergySum(int range)
{
    float sum = 0.f;

    if (range == 0) { //kick
        for (int i = kickMin; i <= kickMax; i++) 
            sum += parent->getFFTDataL()[i] + parent->getFFTDataR()[i];
        sum /= bandKick;
    }
    else if (range == 1) { //snare
        for (int i = snareMin; i <= snareMax; i++)
            sum += parent->getFFTDataL()[i] + parent->getFFTDataR()[i];
        sum /= bandSnare;
    }

    return sum;
}

void BeatDetector::calculateThreshold()
{
    average[SpectrumBand::kick] = calculateAverageQueue(energyHistory,SpectrumBand::kick);
    average[SpectrumBand::snare] = calculateAverageQueue(energyHistory,SpectrumBand::snare);

    variance[SpectrumBand::kick] = calculateEnergyVariance(average[0], energyHistory, SpectrumBand::kick);
    variance[SpectrumBand::snare] = calculateEnergyVariance(average[1], energyHistory, SpectrumBand::snare);

    threshold[SpectrumBand::kick] = -15 * variance[SpectrumBand::kick] + 1.55;
    threshold[SpectrumBand::snare] = -15 * variance[SpectrumBand::snare] + 1.55;

}

float BeatDetector::calculateAverageQueue(std::queue<std::vector<float>> temporalQueue, SpectrumBand index)
{
    int size = temporalQueue.size();
    float sum=0;
    while (temporalQueue.size() > 0)
    {
        sum += temporalQueue.front()[index];
        temporalQueue.pop();
    }
    return sum / size;
}

float BeatDetector::calculateEnergyVariance(float average, std::queue<std::vector<float>> temporalQueue, SpectrumBand index)
{
    int size = temporalQueue.size();
    float sum = 0;
    while (temporalQueue.size() > 0)
    {
        sum += pow((temporalQueue.front()[index] - average), 2);
        temporalQueue.pop();
    }
    return sum / size;
}

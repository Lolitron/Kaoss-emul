/*
  ==============================================================================

    LPF.cpp
    Created: 17 Dec 2021 9:31:41pm
    Author:  ClarVik

  ==============================================================================
*/

#include "LPF.h"

namespace effects {

    void LPF::updateParams(int x, int y)
    {
        //mFilter.state.getObject()->setCutOffFrequency(mSampleRate, 6000 + (y/100.)*-5900,0.71);
        filter.setCutoffFrequency(15000 + (-y / 100.) * 14990);
    } 

    void LPF::prepare(const ProcessSpec& s)
    {
        AudioEffectBase::prepare(s);
        filter.setType(StateVariableTPTFilterType::lowpass);
        filter.prepare(s);
    }

}
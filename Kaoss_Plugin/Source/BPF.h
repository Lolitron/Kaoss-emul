/*
  ==============================================================================

    BPF.h
    Created: 18 Dec 2021 6:13:42pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "FilterBase.h"

namespace effects {

    class BPF : public FilterBase
    {
    public:
        void updateParams(int x, int y) override;
        void prepare(const ProcessSpec& s) override;
        virtual void process(AudioBuffer<float>& buffer) override;
    private:
        DryWetMixer<float> mDryWet;
    };

}
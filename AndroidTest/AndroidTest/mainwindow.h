#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "scribblearea.h"
#include "../../SharedCode/globals.h"
#include "../../SharedCode/timer.h"
#include "effectbutton.h"
#include "chrono"
#include <QButtonGroup>
#include <QMainWindow>
#include <QPushButton>
#include <QQueue>
#include <iostream>
#include <fstream>
#include <QUdpSocket>
#include <QCursor>
#include <QVBoxLayout>
#include <QProxyStyle>
#include <QGraphicsDropShadowEffect>
#include <qtimer.h>
#include <stdio.h>
#include <condition_variable>


QT_BEGIN_NAMESPACE


struct Effect;
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


private slots:

    void on_btnBPM_pressed();

    void on_sbBPM_valueChanged(int arg1);

    void flashBPMButton();

    void everyMs();

    void on_verticalSlider_valueChanged(int value);

    bool isRecording() {return recording;};

    void on_btnRec_clicked();

    void on_RecordBarTick();

    void on_RecordTick();

    void sendAllStates();

private:
    Ui::MainWindow *ui;

    UDPSender* sender;
    ScribbleArea* area;
    QQueue<int> periods;
    timing::Timer* timer;
    QTimer* repaintTimer;
    QTimer* timerBPMFlash;
    QTimer* timerEveryMs;
    QTimer* timerRecordBar;
    QTimer* timerRecordTick;
    int msFlash=0;
    bool recording=false;

    static const int numEffects = btns::ButtonRack::numEffects;
    int buttonOffset;
    QVector<EffectButton*> buttonRack;

};
#endif // MAINWINDOW_H

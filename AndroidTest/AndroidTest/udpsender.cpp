#include "udpsender.h"
#include <chrono>


#include "scribblearea.h"

using namespace std::chrono;
using namespace std;


UDPSender::UDPSender(QWidget *parent) : QWidget(parent)
{
    socket = new QUdpSocket(this);
    socket->bind(PORT, QUdpSocket::BindFlag::ShareAddress);
    connect(socket, &QUdpSocket::readyRead, this, &UDPSender::processPendingDatagrams);
    latencyTimer = timing::Timer();
    send("time 1"); //measure latency
}

void UDPSender::send(string data)
{
    socket->writeDatagram(QByteArray(data.c_str()),QHostAddress::Broadcast,PORT);
}

void UDPSender::processPendingDatagrams()
{
    QByteArray datagram;
    while (socket->hasPendingDatagrams())
    {
        datagram.resize(int(socket->pendingDatagramSize()));
        socket->readDatagram(datagram.data(), datagram.size());
        if(datagram.split(' ')[0]=="beat"){
            beat = datagram.split(' ')[1].toInt();
        }
        else if(datagram.split(' ')[0]=="peak"){
            peak = datagram.split(' ')[1].toInt();
        }
        else if(datagram.split(' ')[0]=="ask"){
            emit refresh();
        }
        else if(datagram.split(' ')[0]=="time"){
            qDebug(std::to_string(latencyTimer.elapsedMs()).c_str());
        }
    }
}


/*
  ==============================================================================

    Looper.cpp
    Created: 13 Dec 2021 1:46:28pm
    Author:  ClarVik

  ==============================================================================
*/

#include "Looper.h"
#include "iostream"

namespace effects {

    void Looper::updateParams(int x, int y)
    {
        if (y == 0)
        {
            mWritePosition = 0;
            mReadPosition = 0;
            mBufferFilled = false;
            loopBuffer.clear();
            mStartedWriting = false;
        }
        mLength = mSampleRate * ((60.f/bpm)*getZone(x/100.));

        if (!mStartedWriting) {
            if (beat && mQuantTimer->elapsedMs() >= (60. / bpm) * 1000 * 16 * 8) //new quad
            {
                mTact = 0;
                mQuantTimer->restart();
                mStartedWriting = y!=0;
            }

            if ((int)mQuantTimer->elapsedMs() % (int)((60. / bpm) * 1000 *2) <= 10 && y!=0)
            {
                mStartedWriting = true;
                mTact++;
            }
        }
        

        mShouldProcess = y != 0;
    }

    void Looper::fillDelayBuffer(int channel, const int bufferLength, const float* bufferData)
    {
        if (mLength > mWritePosition + bufferLength) //we have enough space in delayBuffer to add next block to it 
        {
            loopBuffer.copyFromWithRamp(channel, mWritePosition, bufferData, bufferLength, 1, 1);
        }
        else
        {
            loopBuffer.copyFromWithRamp(channel, mWritePosition, bufferData, mLength - mWritePosition, 1, 1);  //add piece of block that fits
            mBufferFilled = true;
        }
    }

    void Looper::getFromDelayBuffer(juce::AudioBuffer<float>& buffer, int channel, const int bufferLength, const float* delayBufferData)
    {
        if (mReadPosition > mLength)
            mReadPosition = mLength;

        if (mLength > mReadPosition + bufferLength)
        {
            buffer.copyFrom(channel, 0, delayBufferData + mReadPosition, bufferLength); //insert segment in main buffer
        }
        else
        {
            buffer.copyFrom(channel, 0, delayBufferData + mReadPosition, mLength - mReadPosition);   //insert a piece
            buffer.copyFrom(channel, mLength - mReadPosition, delayBufferData, bufferLength - mLength + mReadPosition);  //wrap and add remaining from the segment start
        }
    }

    void Looper::prepare(const ProcessSpec& s)
    {
        AudioEffectBase::prepare(s); 
        const int delayBufferSize = (s.sampleRate + s.maximumBlockSize) * 4;
        mLength = delayBufferSize;
        loopBuffer.setSize(mNumInputChannels, delayBufferSize);
        loopBuffer.clear();
        mQuantTimer = new timing::Timer();
    }

    void Looper::process(AudioBuffer<float>& buffer)
    {
        if (!mShouldProcess || !mStartedWriting) return;

        const int bufferLength = buffer.getNumSamples();

        for (int channel = 0; channel < buffer.getNumChannels(); channel++)
        {
            const float* bufferData = buffer.getReadPointer(channel);
            const float* delayBufferData = loopBuffer.getReadPointer(channel);

            if (!mBufferFilled)
                fillDelayBuffer(channel, bufferLength, bufferData);
            else
                getFromDelayBuffer(buffer, channel, bufferLength, delayBufferData);
        }

        if (!mBufferFilled)
            mWritePosition += bufferLength;
        else {
            mReadPosition += bufferLength;
            mReadPosition %= mLength;
        }
    }

    void Looper::reset()
    {
        mWritePosition = 0;
        mReadPosition = 0;
    }

}
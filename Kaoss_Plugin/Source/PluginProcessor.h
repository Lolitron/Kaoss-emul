/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#ifndef PROC_H
#define PROC_H

#include <JuceHeader.h>
#include "vector"
#include "algorithm"
#include "AllEffects.h"


class UDPServer;
class BeatDetector;
class HandlerProcessorEditor;

class HandlerProcessor : public juce::AudioProcessor, private Timer
{
public:
    //==============================================================================
    HandlerProcessor();
    ~HandlerProcessor() override;

    //==============================================================================
    void prepareToPlay(double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

#ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported(const BusesLayout& layouts) const override;
#endif

    void processBlock(juce::AudioBuffer<float>&, juce::MidiBuffer&) override;

    void pushToFifo(int channel, float sample);

    //update absMin and absMax and normalize (0..1)
    void normalizeFftData(float* data);

    //ask mobile app for all states
    void askRefresh();

    //==============================================================================
    juce::AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const juce::String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram(int index) override;
    const juce::String getProgramName(int index) override;
    void changeProgramName(int index, const juce::String& newName) override;

    //==============================================================================
    void getStateInformation(juce::MemoryBlock& destData) override;
    void setStateInformation(const void* data, int sizeInBytes) override;

    //==============================================================================

    void timerCallback() override;

    //==============================================================================

    void setXY(int x, int y);

    SmoothedValue<float> x;
    SmoothedValue<float> y;
    
    float getSampleRate() { return mSampleRate; }
    float* getFFTDataL() { return mpFftDataL; }
    float* getFFTDataR() { return mpFftDataR; }

    float getBeat() { return beat; }
    void setBeat(float b) { beat=b; }

    void setOn(int num, bool on);
    bool getEffectState(int num);
    void setRecordedCoords(int num, std::pair<int,int> coords) { mEffectRack[num]->setRecordedCoords(coords); }
    void record(int num, std::vector<std::pair<int,int>> interp) { mEffectRack[num]->setRecTape(interp); }
    void unrecord(int num) { mEffectRack[num]->unrecord(); }

    void applyNewBpm(int newBpm);

    void setGain(float newGain) { mGain = newGain; }

    bool getPeak() { return peak; }

private:
    std::vector<effects::AudioEffectBase*> mEffectRack;
    effects::Rate rate;
    effects::Tempo tempo;
    effects::Pitch pitch;
    effects::Looper looper;
    effects::RevLooper rlooper;
    effects::Delay delay;
    effects::SingleDelay sDelay;
    effects::TapeEcho tape;
    effects::Reverberation rever;
    effects::ChorusEffect chorus;
    effects::Decimator decimator;
    effects::Flanger flanger;
    effects::Pan pan;
    effects::CompressorEffect comp;
    effects::HPF hpf;
    effects::LPF lpf;
    effects::BPF bpf;


    UDPServer* server;

    int mSampleRate;
    int bpm;
    float mGain{1};

    //beat detector
    static const int fftSize = 1024 ;
    float mFifoL[fftSize];
    float mFifoR[fftSize];
    int mFifoIndexL{ 0 };
    int mFifoIndexR{ 0 };
    float* mpFftDataL;
    float* mpFftDataR;
    BeatDetector* mpBeatDetector;
    dsp::FFT* mpFFTprocessor;
    bool mNextBlockReady{false};
    float mMinAbs, mMaxAbs;

    float beat;
    bool peak;
    

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(HandlerProcessor)

};


template<class Type> bool contains(std::vector<Type> v, Type val)
{
    return std::find(v.begin(), v.end(), val) != v.end();
}

#endif 
import os
import sys
from time import sleep
import keyboard
from sqlalchemy import true

GET_PATH = ['./../Kaoss_Plugin/Source/','./../AndroidTest/AndroidTest/', './../SharedCode/']
WRITE_FILENAME = True
ALT_SLEEP_TIME = 0.6
SKIP_PRECOMMENT = True
WITH_BORDERS = True

sleep(2)

keyboard.add_hotkey('escape', sys.exit)

for curPath in GET_PATH:
    files = []
    for filename in os.listdir(curPath):
        files.append(filename)

    files.sort(reverse=True)
    files.sort(key=lambda x: x[0])

    for filename in files:
        if filename.split('.')[-1] == "h" or filename.split('.')[-1] == "cpp":
            with open(os.path.join(curPath, filename), 'r') as f:
                data = f.readlines()
                if WRITE_FILENAME:
                    keyboard.send('ctrl+i')
                    keyboard.write(filename)
                    keyboard.send('ctrl+i')
                    keyboard.send('enter')

                if WITH_BORDERS:
                    keyboard.send('alt') # borders on
                    sleep(ALT_SLEEP_TIME)
                    keyboard.send('z')
                    keyboard.send('f')
                    keyboard.send('u')
                    keyboard.send('i')

                keyboard.release('alt')
                skip = None
                one = False
                stop = False
                for line in data:
                    if not stop:
                        if line.find('/*') != -1 and skip==None:
                            skip = True
                        elif line.find('*/') != -1:
                            skip = False
                            one = True
                            continue
                        if skip:
                            continue
                        if one:
                            one=False
                            stop=True
                            continue
                    keyboard.write(line)
                    sleep(0.01)
                sleep(2)

                keyboard.send('alt')
                sleep(ALT_SLEEP_TIME)
                keyboard.send('c')
                keyboard.send('g') #page
                keyboard.send('g')

                if WITH_BORDERS:
                    keyboard.send('alt') # borders off
                    sleep(ALT_SLEEP_TIME)
                    keyboard.send('z')
                    keyboard.send('f')
                    keyboard.send('u')
                    keyboard.send('i')


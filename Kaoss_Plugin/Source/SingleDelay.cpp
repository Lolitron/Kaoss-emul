/*
  ==============================================================================

    SingleDelay.cpp
    Created: 30 Dec 2021 4:38:20pm
    Author:  ClarVik

  ==============================================================================
*/

#include "SingleDelay.h"

namespace effects {

    void SingleDelay::updateParams(int x, int y)
    {
        mDelayFreq = getZone(y / 100.f) * (60. / bpm) * 1000;
        mPanner.setPan(-1+(x/100.f)*2);
        if (x == 0)
            mPanner.setPan(0);
        mShouldProcess = y != 0;
    }

    void SingleDelay::process(AudioBuffer<float>& buffer)
    {
        if (!mShouldProcess)
        {
            delayBuffer.clear();
            return;
        }

        int readPointer, writePointer;

        for (int channel = 0; channel < mNumInputChannels; channel++)
        {
            mpDryBufferData = buffer.getWritePointer(channel);
            mpDelayBufferData = delayBuffer.getWritePointer(channel);
            readPointer = mReadPosition;
            writePointer = mWritePosition;

            for (int i = 0; i < buffer.getNumSamples(); i++)
            {
                const float in = mpDryBufferData[i];
                float out = 0.0;

                out = in + mpDelayBufferData[readPointer];

                mpDelayBufferData[writePointer] = in;

                if (++readPointer >= delayBuffer.getNumSamples())
                    readPointer = 0;
                if (++writePointer >= delayBuffer.getNumSamples())
                    writePointer = 0;

                mpDryBufferData[i] = out;
            }

        }

        mWritePosition = writePointer;
        mReadPosition = (int)(mWritePosition - (mDelayFreq / 1000. * mSampleRate) + delayBuffer.getNumSamples()) % delayBuffer.getNumSamples();
    }


}
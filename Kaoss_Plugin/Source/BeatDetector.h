/*
  ==============================================================================

    BeatDetector.h
    Created: 8 Jan 2022 11:47:21pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "Essentials.h"

#ifndef DETECTOR_H
#define DETECTOR_H

class HandlerProcessor;

class BeatDetector : public Component,
    public Thread
{
public:
    BeatDetector(HandlerProcessor*);

    // ������������ ����� Thread
    virtual void run() override;

private:

    enum SpectrumBand
    {
        kick = 0,
        snare = 1
    };

    void beatDetection();

    float performEnergySum(int range);

    void calculateThreshold();

    /// <summary>
    /// calculate avg(H)
    /// </summary>
    /// <param name="temporalQueue">history</param>
    /// <param name="index">calculate snares or kicks</param>
    /// <returns> avg(H)</returns>
    float calculateAverageQueue(std::queue<std::vector<float>> temporalQueue, SpectrumBand index);
    /// <summary>
    /// variance needed to calculate threshold
    /// </summary>
    /// <param name="average">avg(H)</param>
    /// <param name="temporalQueue">H[] (history)</param>
    /// <param name="index">kick/snare</param>
    /// <returns></returns>
    float calculateEnergyVariance(float average, std::queue<std::vector<float>> temporalQueue, SpectrumBand index);


    HandlerProcessor* parent; 

    int blocksPerSecond;

    float average[2];
    float variance[2];
    float threshold[2];

    float current[2];

    int kickMin, kickMax;
    int snareMin, snareMax;
    int bandKick;
    int bandSnare;
    std::queue<std::vector<float>> energyHistory; //fifo
};


#endif 
/*
  ==============================================================================

    Flanger.cpp
    Created: 6 Mar 2022 3:53:51pm
    Author:  ClarVik

  ==============================================================================
*/

#include "Flanger.h"

void effects::Flanger::updateParams(int x, int y)
{
    mpLfoL->setFrequency(0.01 + (x/100.f)*2.);
    mpLfoR->setFrequency(0.02 + (x/100.f)*2.1);
    mFeedback = (y / 100.f);
}

void effects::Flanger::prepare(const ProcessSpec& s)
{
    AudioEffectBase::prepare(s);
    mpLfoL = new LFO(s.sampleRate, 1);
    mpLfoR = new LFO(s.sampleRate, 1.1);

    mpBufferL = new FractionalDelayBuffer(s.sampleRate);
    mpBufferR = new FractionalDelayBuffer(s.sampleRate);
}

void effects::Flanger::process(AudioBuffer<float>& buffer)
{
    float modL, modR;
    float samplesDelayL, samplesDelayR;
    float dryL, dryR;
    float wetL, wetR;
    float mixL, mixR;

    if (mNumInputChannels == 1) buffer.copyFrom(1, 0, buffer, 0, 0, buffer.getNumSamples()); //in case if mono

    for (int i = 0; i < buffer.getNumSamples(); i++)
    {
        modL = (mpLfoL->tick()+1) * 0.75 * (mSampleRate/100);
        modR = (mpLfoR->tick()+1) * 0.75 * (mSampleRate/100);

        samplesDelayL = 1 * (mSampleRate / 1000) + modL;
        samplesDelayR = 1 * (mSampleRate / 1000) + modR;

        dryL = buffer.getReadPointer(0)[i];
        dryR = buffer.getReadPointer(1)[i];
        
        wetL = (*mpBufferL)[samplesDelayL];
        wetR = (*mpBufferR)[samplesDelayR];

        mixL = dryL + wetL * mFeedback;
        mixR = dryR + wetR * mFeedback;

        mpBufferL->addSample(mixL);
        mpBufferR->addSample(mixR);

        buffer.getWritePointer(0)[i] = mixL;
        buffer.getWritePointer(1)[i] = mixR;
    }
}

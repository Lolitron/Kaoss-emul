/*
  ==============================================================================

    RevLooper.h
    Created: 15 Dec 2021 8:48:43pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "Looper.h"

namespace effects {

    class RevLooper : public Looper
    {
    public:
        void fillDelayBuffer(int channel, const int bufferLength, const float* bufferData) override;

    };

}
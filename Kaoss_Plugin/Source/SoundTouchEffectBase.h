/*
  ==============================================================================

    SoundTouchEffectBase.h
    Created: 16 Dec 2021 11:19:15am
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "AudioEffectBase.h"
#include "soundtouch/include/SoundTouch.h"

namespace effects {

    class SoundTouchEffectBase : public AudioEffectBase
    {
    public:
        
        //inherited
        virtual void prepare(const ProcessSpec& s) override;
        virtual void process(AudioBuffer<float>& buffer) override;
        virtual void reset() override;

    protected:

        float getZone(float n) override;

        soundtouch::SoundTouch* pProcessor1;
        soundtouch::SoundTouch* pProcessor2;
        bool pitcherCleared{true};
        bool tempoCleared{true};
    };

}
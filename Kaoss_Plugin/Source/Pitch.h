/*
  ==============================================================================

    Pitch.h
    Created: 15 Dec 2021 8:55:30pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "Essentials.h"

namespace effects {

    class Pitch : public SoundTouchEffectBase
    {
    public:

        //inherited
        virtual void updateParams(int x, int y) override;

    };

}
/*
  ==============================================================================

    Decimator.cpp
    Created: 18 Dec 2021 6:28:37pm
    Author:  ClarVik

  ==============================================================================
*/

#include "Decimator.h"

namespace effects {

    void Decimator::updateParams(int x, int y)
    {
        mRatio = 20 * (y / 100.);
        mShouldProcess = y != 0;
    }

    void Decimator::prepare(const ProcessSpec& s)
    {
        AudioEffectBase::prepare(s);
        filter.setType(StateVariableTPTFilterType::lowpass);
        filter.prepare(s);
        filter.setCutoffFrequency(5400);
        mRatio = 0;
    }

    void Decimator::process(AudioBuffer<float>& buffer)
    {
        if (!mShouldProcess) return;

        for (int channel = 0; channel < buffer.getNumChannels(); channel++)
        {
            float* channelData = buffer.getWritePointer(channel);
            mSavedSample = channelData[0];
            counter = 0;

            for (int i = 1; i < buffer.getNumSamples(); i++)
            {
                if (counter < mRatio) 
                {
                    channelData[i] = mSavedSample;
                    counter++;
                }
                else 
                {
                    counter = 0;
                    mSavedSample = channelData[i];
                    binApprox(channelData, i,  mRatio+2);
                    
                    if (buffer.getNumSamples() - i < mRatio+2) {
                        binApprox(channelData, buffer.getNumSamples() - 1, buffer.getNumSamples() - i);
                        break;
                    }
                }
            }
        }
        AudioBlock<float> block(buffer);
        ProcessContextReplacing<float> context(block);
        filter.process(context);
    }

    void Decimator::binApprox(float* channelData, int lastIdx, int segmentLength)
    {
        if (segmentLength < 3) return;

        if (segmentLength % 2 == 0)
        {
            channelData[lastIdx - segmentLength / 2] = (channelData[lastIdx - segmentLength] + channelData[lastIdx]) / 2.;
            binApprox(channelData, lastIdx, segmentLength / 2);
            binApprox(channelData, lastIdx-segmentLength/2, segmentLength / 2);
        }
        else 
        {
            channelData[lastIdx - (int)floor(segmentLength / 2)] = (channelData[lastIdx - segmentLength] + channelData[lastIdx]) / 2.;
            channelData[lastIdx - (int)ceil(segmentLength / 2)] = (channelData[lastIdx - segmentLength] + channelData[lastIdx]) / 2.;
            binApprox(channelData, lastIdx - (int)floor(segmentLength / 2), (int)floor(segmentLength / 2));
            binApprox(channelData, lastIdx - (int)ceil(segmentLength / 2), (int)ceil(segmentLength / 2));
        }
    }

}
/*
  ==============================================================================

    Reverberation.h
    Created: 2 Jan 2022 7:31:05pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "Essentials.h"

namespace effects {

    class Reverberation : public AudioEffectBase
    {
    public:
        void updateParams(int x, int y) override;
        void prepare(const ProcessSpec& s) override;
        void process(AudioBuffer<float>& buffer) override;

    private:
        dsp::Reverb mProceccor;
        dsp::Reverb::Parameters mParams;
    };

}
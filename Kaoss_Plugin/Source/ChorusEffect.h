/*
  ==============================================================================

    ChorusEffect.h
    Created: 17 Dec 2021 11:34:04am
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "Essentials.h"

namespace effects {

    class ChorusEffect : public AudioEffectBase
    {
    public:
        void updateParams(int x, int y) override;
        void prepare(const ProcessSpec& s) override;
        void process(AudioBuffer<float>& buffer) override;

    private:

        Chorus<float> mProcessor;
    };
}
/*
  ==============================================================================

    SoundTouchEffectBase.cpp
    Created: 16 Dec 2021 11:19:15am
    Author:  ClarVik

  ==============================================================================
*/

#include "SoundTouchEffectBase.h"

namespace effects {

    void SoundTouchEffectBase::prepare(const ProcessSpec& s)
    {
        AudioEffectBase::prepare(s);
        pProcessor1 = new soundtouch::SoundTouch();
        pProcessor1->setChannels(1);
        pProcessor1->setSampleRate(s.sampleRate);
        pProcessor2 = new soundtouch::SoundTouch();
        pProcessor2->setChannels(1);
        pProcessor2->setSampleRate(s.sampleRate);
    }

    void SoundTouchEffectBase::process(AudioBuffer<float>& buffer)
    {
        if (mShouldProcess) {
            float* bufferData = buffer.getWritePointer(0);

            pProcessor1->putSamples(bufferData, buffer.getNumSamples());
            pProcessor1->receiveSamples(bufferData, buffer.getNumSamples());

            bufferData = buffer.getWritePointer(1);
            pProcessor2->putSamples(bufferData, buffer.getNumSamples());
            pProcessor2->receiveSamples(bufferData, buffer.getNumSamples());
        }
    }

    void SoundTouchEffectBase::reset()
    {
        pProcessor1->clear();
        pProcessor2->clear();
    }

    float SoundTouchEffectBase::getZone(float n)
    {
        if (n >= .45 && n <= .55)
            return .5;
        else if (n < .45)
            return (n+.5) * .5; 
        else
            return .5 + (n - .5);
    }

}
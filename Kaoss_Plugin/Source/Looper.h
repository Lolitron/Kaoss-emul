/*
  ==============================================================================

    Looper.h
    Created: 13 Dec 2021 1:46:28pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "Essentials.h"
#include "C:\Users\ClarVik\source\repos\KAOSS_EMUL\SharedCode\Timer.h"

namespace effects {

	class Looper :
		public AudioEffectBase
	{

	public:
		//inherited
		virtual void updateParams(int x, int y) override;
		void prepare(const ProcessSpec& s) override;
		void process(AudioBuffer<float>& buffer);
		void reset() override;

	protected:

		virtual void fillDelayBuffer(int channel, const int bufferLength, const float* bufferData);
		void getFromDelayBuffer(juce::AudioBuffer<float>& buffer, int channel, const int bufferLength, const float* delayBufferData);

		AudioBuffer <float> loopBuffer;
		int mWritePosition;
		int mReadPosition;
		int mLength;
		bool mBufferFilled{ false };
		int mTact{ 0 };
		bool mStartedWriting{false};
		timing::Timer* mQuantTimer;
	};

}
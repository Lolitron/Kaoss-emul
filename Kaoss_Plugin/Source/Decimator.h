/*
  ==============================================================================

    Decimator.h
    Created: 18 Dec 2021 6:28:37pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "Essentials.h"
#include "LPF.h"

using namespace StateVariableFilter;

namespace effects {

    class Decimator : public AudioEffectBase
    {
    public:
        void updateParams(int x, int y) override;
        void prepare(const ProcessSpec& s) override;
        void process(AudioBuffer<float>& buffer) override;

    private:
        void binApprox(float* channelData, int i, int segmentLength);

        int mRatio{ 0 };
        float counter{ 0 };
        float sum{ 0.0 };
        float mSavedSample;
        float* mFilterKernel;
        StateVariableTPTFilter<float> filter;
    };

}
/*
  ==============================================================================

    LPF.h
    Created: 17 Dec 2021 9:31:41pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "FilterBase.h"

namespace effects {

    class LPF : public FilterBase
    {
    public:
        void updateParams(int x, int y) override;
        void prepare(const ProcessSpec& s) override;
    };

}
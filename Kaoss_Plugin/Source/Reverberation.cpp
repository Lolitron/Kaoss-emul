/*
  ==============================================================================

    Reverberation.cpp
    Created: 2 Jan 2022 7:31:05pm
    Author:  ClarVik

  ==============================================================================
*/

#include "Reverberation.h"

namespace effects {

    void Reverberation::updateParams(int x, int y)
    {
        mParams.roomSize = x / 100.f;
        mParams.dryLevel = 1. - y / 100.f;
        mParams.wetLevel = y / 100.f;
        mProceccor.setParameters(mParams);
    }

    void Reverberation::prepare(const ProcessSpec& s)
    {
        mProceccor.prepare(s);
        mParams = mProceccor.getParameters();
    }

    void Reverberation::process(AudioBuffer<float>& buffer)
    {
        AudioBlock<float> block(buffer);
        ProcessContextReplacing<float> context(block);
        if(mParams.wetLevel != 0)
            mProceccor.process(context);
    }

}
/*
  ==============================================================================

    Rate.cpp
    Created: 7 Feb 2022 2:47:57pm
    Author:  ClarVik

  ==============================================================================
*/

#include "Rate.h"


namespace effects {

    void Rate::updateParams(int x, int y)
    {
        if (y == 0) {
            if (!pProcessor1->isEmpty() && !pitcherCleared)
            {
                pProcessor1->clear();
                pProcessor2->clear();
                pitcherCleared = true;
            }
            pProcessor1->setRateChange(0);
            pProcessor2->setRateChange(0);
        }
        else {
            pitcherCleared = false;
            pProcessor1->setRateChange((float)(-50 + getZone(x / 100.) * 100));
            pProcessor2->setRateChange((float)(-50 + getZone(x / 100.) * 100));
        }

        mShouldProcess = y != 0;
    }

}
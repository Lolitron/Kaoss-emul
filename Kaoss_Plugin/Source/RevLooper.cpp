/*
  ==============================================================================

    RevLooper.cpp
    Created: 15 Dec 2021 8:48:43pm
    Author:  ClarVik

  ==============================================================================
*/

#include "RevLooper.h"

namespace effects {

    void RevLooper::fillDelayBuffer(int channel, const int bufferLength, const float* bufferData)
    {
        if (mLength > mWritePosition + bufferLength) //we have enough space in delayBuffer to add next block to it 
        {
            loopBuffer.copyFromWithRamp(channel, mWritePosition, bufferData, bufferLength, 0.8f, 0.8f);
        }
        else
        {
            const int bufferRemaining = mLength - mWritePosition;
            loopBuffer.copyFromWithRamp(channel, mWritePosition, bufferData, bufferRemaining, 0.8f, 0.8f);  //add piece of block that fits
            mBufferFilled = true;
            loopBuffer.reverse(0, mLength);
        }
    }

}
/*
  ==============================================================================

    Tempo.cpp
    Created: 16 Dec 2021 4:51:32pm
    Author:  ClarVik

  ==============================================================================
*/

#include "Tempo.h"

namespace effects {

    void Tempo::updateParams(int x, int y)
    {
        if (y == 0) {
            if (!pProcessor1->isEmpty() && !tempoCleared)
            {
                pProcessor1->clear();
                pProcessor2->clear();
                tempoCleared = true;
            }
            pProcessor1->setTempoChange(0);
            pProcessor2->setTempoChange(0);
        }
        else {
            tempoCleared = false;
            pProcessor1->setTempoChange((float)(-50 + getZone(x / 100.) * 100));
            pProcessor2->setTempoChange((float)(-50 + getZone(x / 100.) * 100));
        }

        mShouldProcess = y != 0;
    }

}
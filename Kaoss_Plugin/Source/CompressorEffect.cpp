/*
  ==============================================================================

    CompressorEffect.cpp
    Created: 18 Dec 2021 6:59:42pm
    Author:  ClarVik

  ==============================================================================
*/

#include "CompressorEffect.h"

namespace effects {

    void CompressorEffect::updateParams(int x, int y)
    {
        processor.setAttack(1+(x/100.f)*250);
        processor.setThreshold((y / 100.) * -60);
    }

    void CompressorEffect::prepare(const ProcessSpec& s)
    {
        AudioEffectBase::prepare(s);
        processor.prepare(s);
        processor.setRelease(100);
        processor.setRatio(5);
    }

    void CompressorEffect::process(AudioBuffer<float>& buffer)
    {
        AudioBlock<float> block(buffer);
        ProcessContextReplacing<float> context(block);
        processor.process(context);
    }

}

/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include "../../SharedCode/globals.h"

using namespace juce;

//==============================================================================
HandlerProcessorEditor::HandlerProcessorEditor(HandlerProcessor& p)
    : AudioProcessorEditor(&p), audioProcessor(p)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.

    setSize(500, 700);
    int effectButtonWidth = getWidth() / 8;
    int rowLength = 6;
    for (int i = 0; i < btns::ButtonRack::numEffects; i++) 
    {
        effectBtns.push_back(new TextButton(btns::toString((btns::ButtonRack)i)));
        effectBtns[i]->setColour(TextButton::buttonColourId, Colour(32, 32, 32));
        effectBtns[i]->setColour(TextButton::ColourIds::buttonOnColourId, Colour(100, 32, 32));
        effectBtns[i]->setClickingTogglesState(true);
        addAndMakeVisible(effectBtns[i]);
        effectBtns[i]->onClick = [this, i] {
            audioProcessor.setOn(i, effectBtns[i]->getToggleState());
            repaint();
        };
        effectBtns[i]->setBounds(getLocalBounds().withWidth(effectButtonWidth).withX(10+(effectButtonWidth +10)*(i% rowLength)).withY(10+effectButtonWidth *(i/ rowLength)).withHeight(effectButtonWidth));
    }

    getLookAndFeel().setColour(ScrollBar::thumbColourId, Colours::greenyellow);

    addAndMakeVisible(resetBtn);
    resetBtn.setButtonText("Refresh from phone");
    resetBtn.setColour(TextButton::buttonColourId, Colour(32,32,32));
    resetBtn.setClickingTogglesState(false);
    resetBtn.onClick = [this] {
        audioProcessor.askRefresh();
    };

    viewPort.setBounds(10, getBottom() - 350 - 10, 350, 350);
    touchPoint.setBounds(viewPort.getX(), viewPort.getY(), 30, 30);

    addAndMakeVisible(bpmTextBox);
    bpmTextBox.setMultiLine(false, true);
    bpmTextBox.setReturnKeyStartsNewLine(false);
    bpmTextBox.setSelectAllWhenFocused(true);
    bpmTextBox.setCaretVisible(false);
    bpmTextBox.setFont(Font(32));
    bpmTextBox.setJustification(Justification::centred);
    bpmTextBox.setText("200");
    bpmTextBox.onTextChange = [this] {
        effects::AudioEffectBase::bpm = atoi(bpmTextBox.getText().toStdString().c_str());
    };
    bpmTextBox.setBounds(10, effectBtns[effectBtns.size()-1]->getBottom()+10, getWidth()/4, 50);

    addAndMakeVisible(bpmLabel);
    bpmLabel.setFont(Font(32));
    bpmLabel.setText("BPM", NotificationType::dontSendNotification);
    bpmLabel.setBounds(bpmTextBox.getBounds().withX(bpmTextBox.getRight()+10));

    addAndMakeVisible(tapBpm);
    tapBpm.setButtonText("TAP");
    tapBpm.setColour(TextButton::buttonColourId, Colour(32, 32, 32));
    tapBpm.setClickingTogglesState(false);
    tapBpm.onClick = [this] {
        tapPeriods.insert(tapPeriods.begin(),tapTimer.elapsedMs()); //queue simulation
        tapTimer.restart();

        if (tapPeriods.size() > 6) 
            tapPeriods.pop_back();

        float sum = 0.f;
        for (int period : tapPeriods)
            sum += period;
        sum /= tapPeriods.size();

        bpmTextBox.setText(String((int)(10000 * tapPeriods.size() / sum)));
    };
    tapBpm.setBounds(bpmLabel.getBounds().withX(bpmLabel.getRight() + 10));

    addAndMakeVisible(beatLabel);
    beatLabel.setFont(Font(32));
    beatLabel.setJustificationType(Justification::centred);
    beatLabel.setText("BEAT", NotificationType::dontSendNotification);
    beatLabel.setBounds(bpmTextBox.getBounds().withY(bpmTextBox.getBottom()+10));
    beatLabel.setBorderSize(BorderSize<int>(2));
    beatLabel.setColour(Label::outlineColourId, Colours::grey);

    addAndMakeVisible(warningLabel);
    warningLabel.setFont(Font(16));
    warningLabel.setText("Tap your smartphone sensor panel to connect!", NotificationType::dontSendNotification);
    warningLabel.setColour(Label::textColourId, Colours::red);
    warningLabel.setBounds(beatLabel.getBounds().withX(beatLabel.getRight()+10));
    
    addAndMakeVisible(warningBubble);

    addAndMakeVisible(syncToggle);
    syncToggle.setButtonText("Sync with phone");
    syncToggle.setBounds(warningLabel.getBounds().withX(warningLabel.getRight()+10));
    syncToggle.setHelpText("Caution! May cause missed inputs");
    syncToggle.setTooltip("Caution! May cause missed inputs");
    syncToggle.setAccessible(true);
    syncToggle.onClick = [this]() {
        auto msg = AttributedString("Caution! May cause missed inputs");
        msg.setColour(Colours::red);
        warningBubble.showAt(&syncToggle, msg, 1500);
    };
    //hide touch point
    touchPoint.setPosition((int)((audioProcessor.x.getTargetValue() / 100.f) * viewPort.getWidth() + viewPort.getX()) - touchPoint.getWidth() / 2,
        (int)((audioProcessor.y.getTargetValue() / 100.f) * viewPort.getHeight() + viewPort.getY()) - touchPoint.getHeight() / 2);

    startTimerHz(60);
}


HandlerProcessorEditor::~HandlerProcessorEditor()
{
}

//==============================================================================
void HandlerProcessorEditor::paint(juce::Graphics& g)
{
    g.fillAll(getLookAndFeel().findColour(juce::ResizableWindow::backgroundColourId));
    g.setColour(Colours::black);
    g.fillRect(viewPort);
    g.setGradientFill(ColourGradient(Colours::red, viewPort.getX() + 40, viewPort.getY() + 40, Colours::transparentBlack, viewPort.getX(), viewPort.getY(), true));
    g.fillRect(viewPort.getX(), viewPort.getY(), 100., 100.);
    g.setGradientFill(ColourGradient(Colours::red, viewPort.getX() + viewPort.getWidth() - 40, viewPort.getY() + 40, Colours::transparentBlack, viewPort.getX() + viewPort.getWidth(), viewPort.getY(), true));
    g.fillRect(viewPort.getX() + viewPort.getWidth() - 100, viewPort.getY(), 100., 100.);
    g.setGradientFill(ColourGradient(Colours::red, viewPort.getRight() - 40, viewPort.getBottom() - 40, Colours::transparentBlack, viewPort.getRight(), viewPort.getBottom(), true));
    g.fillRect(viewPort.getRight() - 100, viewPort.getBottom()-100, 100., 100.);
    g.setGradientFill(ColourGradient(Colours::red, viewPort.getX() + 40, viewPort.getBottom()-40, Colours::transparentBlack, viewPort.getX(), viewPort.getBottom(), true));
    g.fillRect(viewPort.getX(), viewPort.getBottom() - 100, 100., 100.);
    g.setColour(Colours::red);
    if (touchPoint.getCentre() != viewPort.getPosition()) 
        g.fillEllipse(touchPoint);
    

    if (effectBtns[btns::ButtonRack::btnLooper]->getToggleState() ||
        effectBtns[btns::ButtonRack::btnRevLooper]->getToggleState())
    {
        g.drawLine(viewPort.getX() + viewPort.getWidth() / 16, viewPort.getY(), viewPort.getX() + viewPort.getWidth() / 16, viewPort.getY() + viewPort.getHeight());
        g.drawLine(viewPort.getX() + viewPort.getWidth() / 8, viewPort.getY(), viewPort.getX() + viewPort.getWidth() / 8, viewPort.getY() + viewPort.getHeight());
        g.drawLine(viewPort.getX() + viewPort.getWidth() / 4, viewPort.getY(), viewPort.getX() + viewPort.getWidth() / 4, viewPort.getY() + viewPort.getHeight());
        g.drawLine(viewPort.getX() + viewPort.getWidth() / 2, viewPort.getY(), viewPort.getX() + viewPort.getWidth() / 2, viewPort.getY() + viewPort.getHeight());
        g.drawLine(viewPort.getX() + 3 * viewPort.getWidth() / 4, viewPort.getY(), viewPort.getX() + 3 * viewPort.getWidth() / 4, viewPort.getY() + viewPort.getHeight());
    }

    if (effectBtns[btns::ButtonRack::btnDelay]->getToggleState() ||
        effectBtns[btns::ButtonRack::btnTapeEcho]->getToggleState() ||
        effectBtns[btns::ButtonRack::btnSingleDelay]->getToggleState())
    {
        g.drawLine(viewPort.getX(), viewPort.getY() + viewPort.getHeight() / 16, viewPort.getX() + viewPort.getWidth(), viewPort.getY() + viewPort.getHeight() / 16);
        g.drawLine(viewPort.getX(), viewPort.getY() + viewPort.getHeight() / 8, viewPort.getX() + viewPort.getWidth(), viewPort.getY() + viewPort.getHeight() / 8);
        g.drawLine(viewPort.getX(), viewPort.getY() + viewPort.getHeight() / 4, viewPort.getX() + viewPort.getWidth(), viewPort.getY() + viewPort.getHeight() / 4);
        g.drawLine(viewPort.getX(), viewPort.getY() + viewPort.getHeight() / 2, viewPort.getX() + viewPort.getWidth(), viewPort.getY() + viewPort.getHeight() / 2);
        g.drawLine(viewPort.getX(), viewPort.getY() + 3 * viewPort.getHeight() / 4, viewPort.getX() + viewPort.getWidth(), viewPort.getY() + 3*viewPort.getHeight() / 4);
    }
    if (effectBtns[btns::ButtonRack::btnPitch]->getToggleState() ||
        effectBtns[btns::ButtonRack::btnTempo]->getToggleState() ||
        effectBtns[btns::ButtonRack::btnRate]->getToggleState())
    {
        g.drawLine(viewPort.getX() + viewPort.getWidth() * .45, viewPort.getY(), viewPort.getX() + viewPort.getWidth() * .45,viewPort.getY() +  viewPort.getHeight());
        g.drawLine(viewPort.getX() + viewPort.getWidth() * .55, viewPort.getY(), viewPort.getX() + viewPort.getWidth() * .55,viewPort.getY() +  viewPort.getHeight());
    }
}

void HandlerProcessorEditor::resized()
{
    auto buttonsBounds = getLocalBounds().withWidth(getWidth()/4).withX(3*getWidth()/4).withY(getHeight()-100).reduced(10);
    resetBtn.setBounds(buttonsBounds.withHeight(50));

    this->setResizable(false, false);
}

void HandlerProcessorEditor::mouseDrag(const MouseEvent& event)
{
    setTouchPos(event);
}

void HandlerProcessorEditor::mouseDown(const MouseEvent& event)
{
    setTouchPos(event);
}

void HandlerProcessorEditor::mouseUp(const MouseEvent& event)
{
    if (event.mods.isLeftButtonDown()) 
    {
        touchPoint.setPosition(viewPort.getX()-touchPoint.getWidth()/2, viewPort.getY() - touchPoint.getHeight() / 2);
        audioProcessor.setXY(0, 0);
        repaint();
    }
}

void HandlerProcessorEditor::setTouchPos(const MouseEvent& event)
{
    if (viewPort.contains(event.getPosition().toFloat()) && event.mods.isLeftButtonDown())
    {
        touchPoint.setPosition(event.x - touchPoint.getWidth() / 2, event.y - touchPoint.getHeight() / 2);
        float xRelPos = (event.x - viewPort.getX()) / viewPort.getWidth();
        if (xRelPos < 0) xRelPos = 0;
        if (xRelPos > 1) xRelPos = 1;
        float yRelPos = (event.y - viewPort.getY()) / viewPort.getHeight();
        if (yRelPos < 0) yRelPos = 0;
        if (yRelPos > 1) yRelPos = 1;
        audioProcessor.setXY(xRelPos * 100, yRelPos * 100);
        repaint();
    }
}

void HandlerProcessorEditor::timerCallback()
{
    if (audioProcessor.getBeat())
        beatLabel.setColour(Label::backgroundColourId, Colours::darkred);
    else
        beatLabel.setColour(Label::backgroundColourId, Colours::transparentBlack);

    if (syncToggle.getToggleState()) {
        for (int i = 0; i < effectBtns.size(); i++) {
            if (effectBtns[i]->getToggleState() != audioProcessor.getEffectState(i))
                effectBtns[i]->setToggleState(audioProcessor.getEffectState(i), NotificationType::dontSendNotification);
        }

        if ((int)((audioProcessor.x.getTargetValue() / 100.f) * viewPort.getWidth() + viewPort.getX()) != touchPoint.getCentreX() &&
            (int)((audioProcessor.y.getTargetValue() / 100.f) * viewPort.getHeight() + viewPort.getY()) != touchPoint.getCentreY())
        {
            touchPoint.setPosition((int)((audioProcessor.x.getTargetValue() / 100.f) * viewPort.getWidth() + viewPort.getX()) - touchPoint.getWidth() / 2,
                (int)((audioProcessor.y.getTargetValue() / 100.f) * viewPort.getHeight() + viewPort.getY()) - touchPoint.getHeight() / 2);
            repaint();
        }
    }

    if (String(effects::AudioEffectBase::bpm) != bpmTextBox.getText())
        bpmTextBox.setText(String(effects::AudioEffectBase::bpm));
}



/*
  ==============================================================================

    Pan.h
    Created: 29 Dec 2021 8:56:35pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once


#include "Essentials.h"

namespace effects {

    class Pan : public AudioEffectBase
    {
    public:
        void updateParams(int x, int y) override;
        void prepare(const ProcessSpec& s);
        void process(AudioBuffer<float>& buffer) override;

    private:

        Panner<float> processor;
    };

}
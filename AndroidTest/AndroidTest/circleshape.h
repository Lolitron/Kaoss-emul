#ifndef CIRCLESHAPE_H
#define CIRCLESHAPE_H

#include "../../SharedCode/timer.h"

#include <QWidget>
#include <QGraphicsEffect>
#include <Qt>
#include <QDial>



class CircleShape
{

private:
    timing::Timer* timer;
    int x,y;

public:
    static const int startRadius=50;
    static const int lifetime=400;
    static const Qt::GlobalColor color=Qt::red;


    timing::Timer* getTimer();
    int getX();
    int getY();


    CircleShape(int x,int y);

    CircleShape();

};

#endif // CIRCLESHAPE_H

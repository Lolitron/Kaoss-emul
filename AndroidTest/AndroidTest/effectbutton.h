#ifndef EFFECTBUTTON_H
#define EFFECTBUTTON_H

#include "../../SharedCode/timer.h"

#include <QAbstractButton>
#include <QGraphicsEffect>
#include <QProgressBar>
#include <QVector>
#include <string>
#include <qpushbutton.h>
#include "../../SharedCode/globals.h"


class EffectButton : public QPushButton
{

    Q_OBJECT

signals:
    void recordStarted();
    void unrecorded();

public:
    EffectButton(QRect geometry, std::string text, QWidget* parent, int effNum);

    bool event(QEvent *event) override;

    //returns the next recorded coordinates
    QPair<int,int> getNextPair();
    std::string createRow();
    bool isNowRecording() {return nowRecording;};
    bool isRecorded();

    void unrecord() {recorded.clear(); recorded.shrink_to_fit();};
    //adds a pair of coordinates to recorded
    void addPair(int x, int y);
    void stopRecord();
    void startRecord();

private:

    static const int holdToRecordMs = 1000;

    timing::Timer holdtimer;
    QVector<QPair<int,int>> recorded;
    QGraphicsDropShadowEffect* effect;
    bool nowRecording{false};
    int effectNumber;
    int recReadPosition{-1};

};


#endif // EFFECTBUTTON_H

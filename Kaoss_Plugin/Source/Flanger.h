/*
  ==============================================================================

    Flanger.h
    Created: 6 Mar 2022 3:53:51pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "Essentials.h"
#define _USE_MATH_DEFINES
#include <math.h>

namespace effects {


    class Flanger : public AudioEffectBase
    {
    public:
        
        //inherited
        void updateParams(int x, int y) override;
        void prepare(const ProcessSpec& s) override;
        void process(AudioBuffer<float>& buffer) override;
        

    protected:
        
        class LFO
        {
        public:
            LFO(int sr, double freq) : twoPi(2 * (2 * acos(0.0)))
            {
                mFrequency = freq;
                mSampleRate = sr;
                mPhase = 0.0;
                updateIncrement();
            }

            void setFrequency(double val) { mFrequency = val;  updateIncrement(); }

            float tick()
            {
                float wave = sin(mPhase);
                mPhase += mPhaseIncrement;
                while (mPhase >= twoPi) {
                    mPhase -= twoPi;
                }
                return wave;
            }

        private:
            void updateIncrement()
            {
                mPhaseIncrement = mFrequency * twoPi / mSampleRate;
            }

            const double twoPi;
            double mFrequency;
            double mPhase;
            double mSampleRate;
            double mPhaseIncrement;
        };

        class FractionalDelayBuffer
        {
        public:
            FractionalDelayBuffer(int sr)
            {
                setBufferSize(0.1*sr);
            }

            float operator[](float index) { return getSample(index); }

            void addSample(float sample)
            {
                mIndex = getValidIndex(mIndex);
                mpDelayBuffer[mIndex++] = sample;
            }

        protected:
            int getValidIndex(int index)
            {
                return index % mDelayBufferSize;
            }

            float getSample(float sampleIndex)
            {
                float localIndex = ((float)mIndex - sampleIndex);

                if (localIndex >= mDelayBufferSize) localIndex -= mDelayBufferSize;

                if (localIndex < 0) localIndex += mDelayBufferSize;

                return linearInterpolate(localIndex);
            }

            void setBufferSize(int size)
            {
                if (mpDelayBuffer) {
                    delete[] mpDelayBuffer;
                }

                mDelayBufferSize = size;
                mpDelayBuffer = new float[mDelayBufferSize];
                memset(mpDelayBuffer, 0, mDelayBufferSize * sizeof(float));
            }

            //sometimes we get not an integer index, so we approximate two closest sample values
            float linearInterpolate(float bufferPosition)
            {
                int lower = (int)bufferPosition;
                int upper = (lower + 1) % mDelayBufferSize;
                float difference = bufferPosition - lower;

                return (mpDelayBuffer[upper] * difference) + (mpDelayBuffer[lower] * (1.f - difference));
            }


            int mIndex{ 0 };
            int mDelayBufferSize{ 0 };
            float* mpDelayBuffer{ NULL };
        };

        double mFeedback;
        FractionalDelayBuffer* mpBufferL;
        FractionalDelayBuffer* mpBufferR;
        LFO* mpLfoL;
        LFO* mpLfoR;
    };

}
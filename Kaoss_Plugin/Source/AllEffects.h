/*
  ==============================================================================

    AllEffects.h
    Created: 7 Feb 2022 2:52:18pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "../../SharedCode/Timer.h"
#include "AudioEffectBase.h"
#include "Looper.h"
#include "RevLooper.h"
#include "Pitch.h"
#include "Tempo.h"
#include "ChorusEffect.h"
#include "PhaserEffect.h"
#include "HPF.h"
#include "LPF.h"
#include "CompressorEffect.h"
#include "Decimator.h"
#include "BPF.h"
#include "Delay.h"
#include "Delay.h"
#include "Pan.h"
#include "SingleDelay.h"
#include "Reverberation.h"
#include "TapeEcho.h"
#include "Rate.h"
#include "Flanger.h"

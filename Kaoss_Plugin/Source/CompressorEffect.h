/*
  ==============================================================================

    CompressorEffect.h
    Created: 18 Dec 2021 6:59:42pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "Essentials.h"

namespace effects {

    class CompressorEffect : public AudioEffectBase
    {
    public:
        void updateParams(int x, int y) override;
        void prepare(const ProcessSpec& s) override;
        void process(AudioBuffer<float>& buffer) override;

    private:
        Compressor<float> processor;
    };

}
/*
  ==============================================================================

    FilterBase.h
    Created: 17 Dec 2021 12:18:21pm
    Author:  ClarVik

  ==============================================================================
*/

#pragma once
#include "Essentials.h"

namespace effects {

    using namespace StateVariableFilter;

    class FilterBase : public AudioEffectBase
    {
    public:
        virtual void process(AudioBuffer<float>& buffer) override;

    protected:
        StateVariableTPTFilter<float> filter;
    };

}